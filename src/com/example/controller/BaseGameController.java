package com.example.controller;

import android.hardware.SensorManager;
import android.util.Log;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.example.Constants;
import com.example.base.GameApplication;
import com.example.manager.ResourceManager;
import com.example.model.AvailableDucks;
import com.example.model.BonusElement;
import com.example.model.Duck;
import com.example.scene.GameScene;
import com.example.service.Generator;
import com.example.service.Lives;
import com.example.service.TextWriter;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Win7
 * Date: 8/31/13
 * Time: 11:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class BaseGameController {
    public static final BaseGameController INSTANCE = new BaseGameController();
    private static final String TAG = "BaseGameController";
    public long points;
    public int multiplier;
    public long birdCount;
    public int difficulty;
    public long highScore;
    public boolean isHighScore = false;
    public Constants.GameType gameType;
    public AvailableDucks availableDucks;
    public TimerHandler duckTimerHandler;
    public PhysicsWorld physicsWorld;
    private GameScene mGameScene;
    private TextWriter textWriter;
    private Lives lives;
    private BaseShooterController baseShooterController;

    public static void prepareController(Constants.GameType gameType, GameScene scene) {
        getInstance().gameType = gameType;
        getInstance().mGameScene = scene;
        getInstance().highScore = GameApplication.getINSTANCE().getHighScore(gameType);
        getInstance().physicsWorld = null;
        getInstance().availableDucks = new AvailableDucks(getInstance().mGameScene);
        /*
        ToDo create more game types
        only GAME_NORMAL works for now
         */
        switch (gameType) {
            case GAME_NORMAL:
                getInstance().points = 0;
                getInstance().multiplier = 1;
                getInstance().birdCount = 0;
                getInstance().difficulty = 1;
                getInstance().physicsWorld = new PhysicsWorld(new Vector2(0, SensorManager.GRAVITY_EARTH), true);
                TextWriter.prepareTextWriter(getInstance().mGameScene, ResourceManager.getInstance().gameFont, gameType);
                Lives.prepareLives(getInstance().mGameScene, gameType);
                getInstance().textWriter = TextWriter.getINSTANCE();
                getInstance().lives = Lives.getINSTANCE();
                getInstance().lives.setLives(3);
                getInstance().isHighScore = false;

                //ShooterControllerInit
                BaseShooterController.prepareBaseShooter(getInstance().mGameScene);
                getInstance().baseShooterController = BaseShooterController.getINSTANCE();
                break;
            case GAME_ARCADE:
                break;
        }
        getInstance().mGameScene.registerUpdateHandler(getInstance().physicsWorld);
        getInstance().setHeaderAndFooter();
    }

    public static BaseGameController getInstance() {
        return INSTANCE;
    }

    public void restart() {
        getInstance().highScore = GameApplication.getINSTANCE().getHighScore(gameType);
        availableDucks.removeAllDucks();

        switch (gameType) {
            case GAME_NORMAL:
                getInstance().points = 0;
                getInstance().multiplier = 1;
                getInstance().birdCount = 0;
                getInstance().difficulty = 1;
                textWriter.restart();
                getInstance().lives.setLives(3);
                getInstance().isHighScore = false;

                getInstance().baseShooterController.destroyWeapon();
                //ShooterControllerInit
                BaseShooterController.prepareBaseShooter(getInstance().mGameScene);
                getInstance().baseShooterController = BaseShooterController.getINSTANCE();
                break;
            case GAME_ARCADE:
                break;
        }
        ResourceManager.getInstance().engine.unregisterUpdateHandler(duckTimerHandler);
        getInstance().setHeaderAndFooter();
        startGame();
    }

    public void birdCreated(Duck duck) {
        availableDucks.add(duck);
        getInstance().birdCount++;

        /*
        give a live
         */
        if (birdCount % Constants.PLUS_LIVE_BIRD_COUNT == 0) {
            lives.plusLive();
        }
        //Log.i("birdCreated", " " + birdCount);
        if (difficulty <= 5) {
            if (birdCount % 10 == 0) {
                if (difficulty < 35) {
                    difficulty++;
                    calculateMultiplier();
                    Log.i("birdCounter", "birds:" + birdCount + " difficulty:" + difficulty + ", multiplier:" + multiplier);
                }
            }
        } else {
            if (difficulty > 5 && difficulty <= 13) {  //on 60 will go to lvl 6
                if (birdCount % 15 == 0) {
                    if (difficulty < 35) {
                        difficulty++;
                        calculateMultiplier();
                        Log.i("birdCounter", "birds:" + birdCount + " difficulty:" + difficulty + ", multiplier:" + multiplier);
                    }
                }
            } else {
                if (difficulty > 13 && difficulty <= 23) {
                    if (birdCount % 23 == 0) {
                        if (difficulty < 35) {
                            difficulty++;
                            calculateMultiplier();
                            Log.i("birdCounter", "birds:" + birdCount + " difficulty:" + difficulty + ", multiplier:" + multiplier);
                        }
                    }
                } else {
                    if (difficulty > 23 && difficulty <= 33) {
                        if (birdCount % 52 == 0) {
                            if (difficulty < 35) {
                                difficulty++;
                                calculateMultiplier();
                                Log.i("birdCounter", "birds:" + birdCount + " difficulty:" + difficulty + ", multiplier:" + multiplier);
                            }
                        }
                    } else {
                        if (birdCount % 100 == 0) {
                            if (difficulty < 35) {
                                difficulty++;
                                calculateMultiplier();
                                Log.i("birdCounter", "birds:" + birdCount + " difficulty:" + difficulty + ", multiplier:" + multiplier);
                            }
                        }

                    }

                }
            }
        }
    }

    public void calculateMultiplier() {
        multiplier = ((difficulty) / 5);
        if (multiplier == 0) {
            multiplier = 1;
        }
        TextWriter.getINSTANCE().writeMultiplier(multiplier);
    }

    public int birdKilled(Duck duck) {
        int birdHeight = duck.DuckHeight;
        int birdType = duck.duckNumber;
        availableDucks.killDuck(duck);
        int point = 1;
        if (birdHeight != 0) {
            //int point = (((135 - birdheight) * difficulty * birdtype) / 10) / 10;
            //TODO implement birdtype here
            if (point == 0) {
                point = 1;
            }
            switch (birdType) {
                case 1: {
                    point = 1;
                    break;
                }
                case 2: {
                    point = 2;
                    break;
                }
                case 3: {
                    point = 5;
                    break;
                }
                case 4: {
                    point = 10;
                    break;
                }
                case 5: {
                    point = 15;
                    break;
                }
            }

            points = points + (point * multiplier);
            //Log.i("Points","points" + points);
        }
        if (points > highScore) {
            isHighScore = true;
            GameApplication.getINSTANCE().saveHighScore(points, gameType);
            TextWriter.getINSTANCE().writeHighScore(points);
        }
        TextWriter.getINSTANCE().writePoints(points, isHighScore);
        return (point * multiplier);
    }

    public void addPoints(long plusPoints) {
        points = points + plusPoints;
        if (points > highScore) {
            isHighScore = true;
            GameApplication.getINSTANCE().saveHighScore(points, gameType);
            TextWriter.getINSTANCE().writeHighScore(points);
        }
        TextWriter.getINSTANCE().writePoints(points, isHighScore);
    }

    public void birdPassed(Duck duck) {
        availableDucks.killDuck(duck);
        lives.minusLive();
        Log.i(TAG, "invoked birdPassed()");
        if (lives.equals(0)) {
            //TODO end game here // make it more fancy
            if (isHighScore && highScore < points) {
                GameApplication.getINSTANCE().saveHighScore(points, gameType);
            }
            ResourceManager.getInstance().engine.unregisterUpdateHandler(duckTimerHandler);
            /*
            //todo set the end child scene
             */
            ((GameScene) mGameScene).setChildSceneByID(Constants.subSceneType.sub_THE_END);
            Log.i(TAG, "THE END");
        }
    }

    public float createTimer() {
        float timer = 0.1f;
        Random random = new Random();
        switch (difficulty) {
//            case 1:{
//                timer = 3.8f - random.nextFloat();
//                break;
//            }
//            case 2: {
//                timer = 3.5f - random.nextFloat();
//                break;
//            }
//            case 3: {
//                timer = 3.2f - random.nextFloat();
//                break;
//            }
//            case 4: {
//                timer = 2.9f - random.nextFloat();
//                break;
//            }
//            case 5: {
//                timer = 2.7f - random.nextFloat();
//                break;
//            }
//            case 6: {
//                timer = 2.5f - random.nextFloat();
//                break;
//            }
//            case 7: {
//                timer = 2.4f - random.nextFloat();
//                break;
//            }
//            case 8: {
//                timer = 2.3f - random.nextFloat();
//                break;
//            }
//            case 9: {
//                timer = 2.2f - random.nextFloat();
//                break;
//            }
//            case 10: {
//                timer = 2.1f - random.nextFloat();
//                break;
//            }
            case 11: {
                timer = 2.0f - random.nextFloat(); //minimum here is 1 second spawn
                break;
            }
            case 12: {
                timer = 1.9f - ((float) random.nextInt(10) / 10.f);   //min=0.9, max=1.9
                break;
            }
            case 13: {
                timer = 1.8f - ((float) random.nextInt(9) / 10.f);   //min=0.9, max=1.8
                break;
            }
            case 14: {
                timer = 1.8f - ((float) random.nextInt(10) / 10.f);   //min=0.8, max=1.8
                break;
            }
            case 15: {
                timer = 1.7f - ((float) random.nextInt(9) / 10.f);   //min=0.8, max=1.7
                break;
            }
            case 16: {
                timer = 1.6f - ((float) random.nextInt(8) / 10.f);   //min=0.8, max=1.6
                break;
            }
            case 17: {
                timer = 1.5f - ((float) random.nextInt(7) / 10.f);   //min=0.8, max=1.4
                break;
            }
            case 18: {
                timer = 1.5f - ((float) random.nextInt(8) / 10.f);   //min=0.7, max=1.5
                break;
            }
            case 19: {
                timer = 1.5f - ((float) random.nextInt(9) / 10.f);   //min=0.6, max=1.5
                break;
            }
            case 20: {
                timer = 1.5f - ((float) random.nextInt(10) / 10.f);   //min=0.5, max=1.5
                break;
            }
            case 21: {
                timer = 1.4f - ((float) random.nextInt(9) / 10.f);   //min=0.5, max=1.4
                break;
            }
            case 22: {
                timer = 1.3f - ((float) random.nextInt(8) / 10.f);   //min=0.5, max=1.3
                break;
            }
            case 23: {
                timer = 1.2f - ((float) random.nextInt(7) / 10.f);   //min=0.5, max=1.2
                break;
            }
            case 24: {
                timer = 1.1f - ((float) random.nextInt(6) / 10.f);   //min=0.5, max=1.1
                break;
            }
            case 25: {
                timer = 1.1f - ((float) random.nextInt(7) / 10.f);   //min=0.4, max=1.1
                break;
            }
            case 26: {
                timer = 1.0f - ((float) random.nextInt(5) / 10.f);   //min=0.5, max=1.0
                break;
            }
            case 27: {
                timer = 1.0f - ((float) random.nextInt(6) / 10.f);   //min=0.4, max=1.0
                break;
            }
            case 28: {
                timer = 1.0f - ((float) random.nextInt(7) / 10.f);   //min=0.3, max=1.0
                break;
            }
            case 29: {
                timer = 1.0f - ((float) random.nextInt(8) / 10.f);   //min=0.2, max=1.0
                break;
            }
            case 30: {
                timer = 1.0f - ((float) random.nextInt(9) / 10.f);   //min=0.1, max=1.0
                break;
            }
            case 31: {
                timer = 0.9f - ((float) random.nextInt(8) / 10.f);   //min=0.1, max=0.9
                break;
            }
            case 32: {
                timer = 0.8f - ((float) random.nextInt(7) / 10.f);   //min=0.1, max=0.8
                break;
            }
            case 33: {
                timer = 0.7f - ((float) random.nextInt(6) / 10.f);   //min=0.1, max=0.7
                break;
            }
            case 34: {
                timer = 0.6f - ((float) random.nextInt(5) / 10.f);   //min=0.1, max=0.6
                break;
            }
            case 35: {
                timer = 0.5f - ((float) random.nextInt(4) / 10.f);   //min=0.1, max=0.5
                break;
            }
            default: {
                timer = 2.0f - random.nextFloat();
                break;
            }
        }


        return timer;
    }

    public int createSpeed() {
        Random random = new Random();
        int newSpeed = Math.round(8 * (float) Constants.CAMERA_WIDTH / (float) Constants.OLD_CAMERA_WIDTH);
        if (difficulty < 11) {
            newSpeed = newSpeed + random.nextInt(8);   //dif[1,10] speed 5,9
        } else {
            if (difficulty < 21) {
                newSpeed = newSpeed + random.nextInt(12) + 1;  //dif[1,10] speed 6, 13
            } else {
                if (difficulty < 31) {
                    newSpeed = newSpeed + random.nextInt(12) + 2;  //dif[1,10] speed 7, 13
                } else {
                    newSpeed = newSpeed + random.nextInt(14) + 5;  //dif[1,10] speed 10, 17
                }
            }
        }
        return newSpeed;
    }

    /**
     * starts the duck creator
     */
    public void startGame() {
        createDucks();
    }

    private void createDucks() {
        float timer = this.createTimer();
        duckTimerHandler = new TimerHandler(timer, false, new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                Random random = new Random();
                //randomly generated duck number
                //int duckNumber = Generator.generateDuckNumber(5, random);
                int duckNumber = 6;

                //int duckCases = random.nextInt(100);
                int duckCases = 95;       //testing purpose only
                //int duckNumber = 4;
                if (duckCases < 60) {
                    //load One Duck
                    Duck duck = new Duck(ResourceManager.getInstance().duckTextureList.get(duckNumber), duckNumber);
                    duck.createEssentials();
                    duck.attachDuck();
                }
                if (duckCases >= 60 && duckCases < 80) {
                    //loadTwo ducks
                    Duck duck = new Duck(ResourceManager.getInstance().duckTextureList.get(duckNumber), duckNumber);
                    duck.createEssentials();
                    duck.attachDuck();
                    Duck duck1 = new Duck(ResourceManager.getInstance().duckTextureList.get(duckNumber), duckNumber);
                    duck1.createEssentials();
                    duck1.attachDuck();
                }
                if (duckCases >= 80 && duckCases < 93) {
                    //load one behind other 3 ducks whereX=random.nextInt(600);
                    duckNumber = Generator.generateDuckNumber(2, random);
                    Duck duck = new Duck(ResourceManager.getInstance().duckTextureList.get(duckNumber), duckNumber);
                    duck.setUpdateMovement(false);
                    duck.createEssentials();
                    int whereX = duck.getWhereX();
                    int whereY = duck.getWhereY();
                    int fleetspeed = duck.getSpeed();
                    int duckWidth = duck.getDuckWidth();
                    int duckHeight = duck.getDuckHeight();


                    Duck duck1 = new Duck(ResourceManager.getInstance().duckTextureList.get(duckNumber), duckNumber);
                    duck1.setUpForFleet(whereX - (duckWidth + 20), whereY, fleetspeed, duckWidth, duckHeight);
                    duck1.createEssentials();


                    Duck duck2 = new Duck(ResourceManager.getInstance().duckTextureList.get(duckNumber), duckNumber);
                    duck2.setUpForFleet(whereX - 2 * (duckWidth + 20), whereY, fleetspeed, duckWidth, duckHeight);
                    duck2.createEssentials();

                    duck.attachDuck();
                    duck1.attachDuck();
                    duck2.attachDuck();
                }
                if (duckCases >= 93 && duckCases < 100) {
                    int weaponTypeInt = random.nextInt(3);
                    Constants.WeaponType weaponType = Constants.WeaponType.Rifle;
                    switch (weaponTypeInt) {
                        case 0:
                            weaponType = Constants.WeaponType.Beretta;
                            break;
                        case 1:
                            weaponType = Constants.WeaponType.Cabirne;
                            break;
                        case 2:
                            weaponType = Constants.WeaponType.Rifle;
                            break;
                    }
                    BonusElement bonusElement = new BonusElement(weaponType);
                    bonusElement.createEssentials();
                    bonusElement.attachElement();

                }
                createDucks();
            }
        });
        ResourceManager.getInstance().engine.registerUpdateHandler(duckTimerHandler);
    }

    public void pauseGame() {
        Log.i(TAG, "invoked pauseGame");
        ResourceManager.getInstance().engine.unregisterUpdateHandler(duckTimerHandler);
    }

    private void setHeaderAndFooter() {
        FixtureDef fixtureDef = PhysicsFactory.createFixtureDef(1.0f, 1.0f, 1.0f);
        Body bodyTop = PhysicsFactory.createBoxBody(physicsWorld, mGameScene.headerSprite,
                BodyDef.BodyType.StaticBody, fixtureDef);
        physicsWorld.registerPhysicsConnector(new PhysicsConnector(mGameScene.headerSprite, bodyTop));
        Body bodyBottom = PhysicsFactory.createBoxBody(physicsWorld, mGameScene.footerSprite,
                BodyDef.BodyType.StaticBody, fixtureDef);
        physicsWorld.registerPhysicsConnector(new PhysicsConnector(mGameScene.footerSprite, bodyBottom));
    }

    public void addMultyKills(Constants.MultiKillType killType) {
        Log.i(TAG, "killType" + killType);
        int bonusPoints = 30;
        switch (killType) {
            case DOUBLE_KILL:
                bonusPoints = 30;
                break;
            case TRIPLE_KILL:
                bonusPoints = 50;
                break;
            case QUADRA_KILL:
                bonusPoints = 100;
                break;
        }
        this.addPoints(bonusPoints);
        textWriter.writeMultiKillPoints(bonusPoints, killType);
    }
}
