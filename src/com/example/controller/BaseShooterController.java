package com.example.controller;

import com.example.Constants;
import com.example.manager.ResourceManager;
import com.example.model.weapons.Beretta;
import com.example.model.weapons.Cabirne;
import com.example.model.weapons.Rifle;
import com.example.model.weapons.Weapon;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.input.touch.TouchEvent;

/**
 * Created with IntelliJ IDEA.
 * User: ALabrosk
 * Date: 9/5/13
 * Time: 10:14 AM
 */
public class BaseShooterController {
    public static final String TAG = "BaseShooterController";
    public static BaseShooterController INSTANCE = new BaseShooterController();

    private Scene mScene;
    private Weapon weaponRifle;
    private Weapon weaponCabirne;
    private Weapon weaponBeretta;

    private Weapon activeWeapon;

    public static BaseShooterController getINSTANCE() {
        return INSTANCE;
    }

    public static void prepareBaseShooter(Scene mScene) {
        getINSTANCE().mScene = mScene;
        getINSTANCE().initWeapons();
        getINSTANCE().mScene.setOnSceneTouchListener(new IOnSceneTouchListener() {
            @Override
            public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
                getINSTANCE().activeWeapon.shoot(pSceneTouchEvent);
                return false;  //To change body of implemented methods use File | Settings | File Templates.
            }
        });
    }

    public void initWeapons() {
        weaponRifle = new Rifle(this.mScene,
                ResourceManager.getInstance().weaponsDisplayMap.get(Constants.WeaponHuntingRifle));
        weaponCabirne = new Cabirne(this.mScene, ResourceManager.getInstance().weaponsDisplayMap.get(Constants
                .WeaponCabarne));
        weaponBeretta = new Beretta(this.mScene, ResourceManager.getInstance().weaponsDisplayMap.get(Constants
                .WeaponBeretta));

        activeWeapon = weaponBeretta;
        activeWeapon.displayWeapon();
    }

    public void changeWeapon(Weapon bWeapon) {
        this.activeWeapon.destroyWeapon();
        this.activeWeapon = bWeapon;
        this.activeWeapon.displayWeapon();
    }

    public void changeWeapon(Constants.WeaponType weaponType){
        switch (weaponType) {
            case Beretta:
                changeWeapon(weaponBeretta);
                break;
            case Cabirne:
                changeWeapon(weaponCabirne);
                break;
            case Rifle:
                changeWeapon(weaponRifle);
                break;
        }
    }

    public void destroyWeapon() {
        this.activeWeapon.destroyWeapon();
    }


}
