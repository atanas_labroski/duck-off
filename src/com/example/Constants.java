package com.example;

/**
 * Created with IntelliJ IDEA.
 * User: Win7
 * Date: 8/29/13
 * Time: 2:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class Constants {
    public static final int OLD_CAMERA_HEIGHT = 480;
    public static final int OLD_CAMERA_WIDTH = 800;

    public static float CAMERA_HEIGHT = 480;
    public static float CAMERA_WIDTH = 800;

    public static float HEADER_BORDER = 32;
    public static float HEADER_BORDER_TEXT_Y = 1;
    public static float FOOTER_BORDER = 32;

    public static float HW = 480/800;

    public static int SubSceneID_PAUSE = 1;
    public static int SubSceneID_THEEND = 2;


    public static final String FONT_NAME = "capture_it.ttf";  //"droid.ttf";
    public static int FONT_SIZE_DEFAULT = 18;
    public static int FONT_SIZE_THE_END = 34;
    public static int FONT_SIZE_GAME_HEADER = 27;
    public static int FONT_SIZE_GAME = 24;

    public static final String GameArcade = "arcadeGame";
    public static final String GameNormal = "normalGame";
    public static final String GamePreferencesHS = "dgmprefs";

    public static final int GoTop = 01;
    public static final int GoBottom = 02;
    public static final int GoStraight = 03;

    public static final String BTN_PLAY = "btn_play";
    public static final String BTN_PAUSE = "btn_pause";
    public static final String BTN_REPLAY = "btn_replay";

    public static final String WeaponHuntingRifle = "weapon01";
    public static final String WeaponCabarne = "weapon02";
    public static final String WeaponBeretta = "weapon03";
    public static final float WeaponDivider = 4;

    public static final int PLUS_LIVE_BIRD_COUNT = 50;


    public enum WeaponType {
        Rifle,
        Cabirne,
        Beretta
    }

    public enum subSceneType {
        sub_PAUSE,
        sub_THE_END,
    }

    public enum SceneType {
        SCENE_SPLASH,
        SCENE_MENU,
        SCENE_GAME,
        SCENE_LOADING,
    }

    public enum GameType {
        GAME_ARCADE,
        GAME_NORMAL,
    }

    public enum MultiKillType {
        DOUBLE_KILL,
        TRIPLE_KILL,
        QUADRA_KILL,
    }
}
