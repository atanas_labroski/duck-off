package com.example;

import org.andengine.util.color.Color;

/**
 * Created with IntelliJ IDEA.
 * User: ALabrosk
 * Date: 9/5/13
 * Time: 10:43 PM
 */
public class ColorsPack {
    public static final Color green = new Color(0.2f,0.7f,0.3f);
    public static final Color red = new Color(0.7f, 0.1f, 0.1f);
    public static final Color grey = new Color(0.08f, 0.08f, 0.08f);
}
