package com.example.manager;

import org.andengine.engine.Engine;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;

/**
 * Created with IntelliJ IDEA.
 * User: Win7
 * Date: 8/31/13
 * Time: 11:28 AM
 * To change this template use File | Settings | File Templates.
 */
public class HandlerManager {

    private static final HandlerManager INSTANCE = new HandlerManager();

    private Engine engine = ResourceManager.getInstance().engine;

    /**
     * loads all menu and game resources
     */
    public void loadingHandler(){
        engine.registerUpdateHandler(new TimerHandler(0.01f, new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                engine.unregisterUpdateHandler(pTimerHandler);
                ResourceManager.getInstance().loadGameResources();
                ResourceManager.getInstance().loadMenuResources();
//                //load scenes
//                try {
//                    Thread.sleep(1000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//                }


                //TODO load the menu scene
                //SceneManager.getInstance().createMenuScene();
                SceneManager.getInstance().disposeLoadingScene();
                SceneManager.getInstance().createGameScene();
            }
        }));
    }

    public static HandlerManager getInstance() {
        return INSTANCE;
    }

}
