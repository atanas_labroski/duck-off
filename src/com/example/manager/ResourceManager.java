package com.example.manager;

import android.util.Log;
import com.example.Constants;
import com.example.base.MainGameActivity;
import com.example.model.DuckTexture;
import org.andengine.engine.Engine;
import org.andengine.engine.camera.SmoothCamera;
import org.andengine.opengl.font.Font;
import org.andengine.opengl.font.FontFactory;
import org.andengine.opengl.texture.ITexture;
import org.andengine.opengl.texture.TextureOptions;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.BitmapTextureAtlasTextureRegionFactory;
import org.andengine.opengl.texture.atlas.bitmap.BuildableBitmapTextureAtlas;
import org.andengine.opengl.texture.atlas.bitmap.source.IBitmapTextureAtlasSource;
import org.andengine.opengl.texture.atlas.buildable.builder.BlackPawnTextureAtlasBuilder;
import org.andengine.opengl.texture.atlas.buildable.builder.ITextureAtlasBuilder;
import org.andengine.opengl.texture.region.ITextureRegion;
import org.andengine.opengl.texture.region.TiledTextureRegion;
import org.andengine.opengl.vbo.VertexBufferObjectManager;
import org.andengine.util.color.Color;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Win7
 * Date: 8/29/13
 * Time: 2:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class ResourceManager {
    private static final String TAG = "ResourceManager";
    private static final ResourceManager INSTANCE = new ResourceManager();
    public Engine engine;
    public MainGameActivity activity;
    public SmoothCamera camera;
    public VertexBufferObjectManager vbom;
    /*
    GAME RESOURCES LOADING
     */
    public List<DuckTexture> duckTextureList = new ArrayList<DuckTexture>();
    public TiledTextureRegion mParallaxLayerBack, mParallaxLayerMid, mParallaxLayerFront, mHeaderTTR;
    public Font gameFont;
    public List<TiledTextureRegion> livesList = new ArrayList<TiledTextureRegion>();
    public HashMap<String, TiledTextureRegion> buttonsMAP = new HashMap<String, TiledTextureRegion>();
    public HashMap<String, ITextureRegion> weaponsDisplayMap = new HashMap<String, ITextureRegion>();
    public HashMap<String, ITextureRegion> weaponsShotMap = new HashMap<String, ITextureRegion>();

    public static void prepareManager(Engine engine, MainGameActivity mainGameActivity,
                                      SmoothCamera camera, VertexBufferObjectManager vbom) {
        getInstance().engine = engine;
        getInstance().activity = mainGameActivity;
        getInstance().camera = camera;
        getInstance().vbom = vbom;
        setMainDirectories();
    }

    /**
     * setting the BasePath -- gfx, font
     */
    private static void setMainDirectories() {
        BitmapTextureAtlasTextureRegionFactory.setAssetBasePath("gfx/");
        FontFactory.setAssetBasePath("font/");
    }

    public static ResourceManager getInstance() {
        return INSTANCE;
    }

    public void loadMenuResources() {
        loadMenuGraphics();
        loadMenuAudio();
    }

    public void loadGameResources() {
        loadGameGraphics();
        loadGameFonts();
        loadGameAudio();
    }

    public void loadMenuGraphics() {

    }

    public void loadMenuAudio() {

    }

    /**
     * loading all game graphics
     */
    public void loadGameGraphics() {
        //loading background goes first
        loadGameBackground();

        /*
        Loading the ducks textures
         */
        loadAllDucksTTR();

        /*
        Loading the lives textures
         */
        loadLivesTTR();

        /*
        Loading the buttons
         */
        loadGameButtonsTTR();

        /*
        Loading the weapons
         */
        loadWeaponsTTR();


    }

    private void loadWeaponsTTR() {
        BitmapTextureAtlas BBTAWeapons = new BitmapTextureAtlas(engine.getTextureManager(), 256,
                196);
        ITextureRegion weaponDisplayITR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(BBTAWeapons,
                activity, "gun.png", 0, 0);
        ITextureRegion weaponCabarneDisplayITR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(BBTAWeapons,
                activity, "cabarne.png", 0, 64);
        ITextureRegion weaponBerettaDisplayITR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(BBTAWeapons,
                activity, "beretta.png", 0, 128);
        weaponsDisplayMap.put(Constants.WeaponHuntingRifle, weaponDisplayITR);
        weaponsDisplayMap.put(Constants.WeaponCabarne, weaponCabarneDisplayITR);
        weaponsDisplayMap.put(Constants.WeaponBeretta, weaponBerettaDisplayITR);

        try {
        BBTAWeapons.load();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        BitmapTextureAtlas BBTAShots = new BitmapTextureAtlas(engine.getTextureManager(), 196, 128);
        ITextureRegion shotITR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(BBTAShots, activity,
                "rifleshot01.png", 0, 0);
        ITextureRegion shotSmallITR = BitmapTextureAtlasTextureRegionFactory.createFromAsset(BBTAShots, activity,
                "helloDucksBullet.png", 128, 0);
        weaponsShotMap.put(Constants.WeaponHuntingRifle, shotITR);
        weaponsShotMap.put(Constants.WeaponCabarne, shotSmallITR);
        try {
            BBTAShots.load();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    /**
     * loads the Parallax Game Background
     */
    public void loadGameBackground() {
        BuildableBitmapTextureAtlas mAutoParallaxBackgroundSunTexture = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                1920, 640, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BuildableBitmapTextureAtlas mAutoParallaxBackgroundCloudsTexture = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                1920, 85, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
//        this.mParallaxLayerFront = BitmapTextureAtlasTextureRegionFactory.createFromAsset(
//                this.mAutoParallaxBackgroundTexture, this, "parallax_background_layer_front.png", 0, 0);
        this.mParallaxLayerBack = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
                mAutoParallaxBackgroundSunTexture, activity, "parallax_background_layer_backhuge.png", 1, 1);   //188       //882
        this.mParallaxLayerMid = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
                mAutoParallaxBackgroundCloudsTexture, activity, "parallax_background_layer_mid.png", 1, 1);   //669     //1497


        BuildableBitmapTextureAtlas headerBBTA = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                800, 50, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        this.mHeaderTTR = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
                headerBBTA, activity, "gameHeader.png", 1, 1);

        try {
            mAutoParallaxBackgroundCloudsTexture.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
            mAutoParallaxBackgroundSunTexture.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
            mAutoParallaxBackgroundCloudsTexture.load();
            mAutoParallaxBackgroundSunTexture.load();

            headerBBTA.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
            headerBBTA.load();
        } catch (ITextureAtlasBuilder.TextureAtlasBuilderException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void loadLivesTTR() {
        //livesLoadGoesHere
        BuildableBitmapTextureAtlas BBTALives0 = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                384, 76, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BuildableBitmapTextureAtlas BBTALives1 = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                384, 76, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BuildableBitmapTextureAtlas BBTALives2 = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                384, 76, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BuildableBitmapTextureAtlas BBTALives3 = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                384, 76, TextureOptions.BILINEAR_PREMULTIPLYALPHA);

        TiledTextureRegion lives0TTR = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(BBTALives0,
                activity, "0lives.png", 1, 1);
        TiledTextureRegion lives1TTR = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(BBTALives1,
                activity, "1lives.png", 1, 1);
        TiledTextureRegion lives2TTR = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(BBTALives2,
                activity, "2lives.png", 1, 1);
        TiledTextureRegion lives3TTR = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(BBTALives3,
                activity, "3lives.png", 1, 1);

        livesList.add(lives0TTR);
        livesList.add(lives1TTR);
        livesList.add(lives2TTR);
        livesList.add(lives3TTR);

        try {
            BBTALives0.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
            BBTALives1.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
            BBTALives2.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
            BBTALives3.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));

            BBTALives0.load();
            BBTALives1.load();
            BBTALives2.load();
            BBTALives3.load();
        } catch (ITextureAtlasBuilder.TextureAtlasBuilderException e) {
            e.printStackTrace();
        }
    }

    public void loadGameFonts() {
        ITexture mFontTextureAtlas = new BitmapTextureAtlas(engine.getTextureManager(), 256, 256, TextureOptions.BILINEAR);
        gameFont = FontFactory.createFromAsset(engine.getFontManager(), mFontTextureAtlas,
                activity.getAssets(), Constants.FONT_NAME, 18, true, Color.WHITE_ABGR_PACKED_INT);  // 6,47,79
        try {
            gameFont.load();
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.i(TAG, "gameFont failed to load");
        }
    }

    public void loadGameButtonsTTR() {
        BuildableBitmapTextureAtlas btnPlayTexture = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                64, 56, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BuildableBitmapTextureAtlas btnPauseTexture = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                64, 56, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        BuildableBitmapTextureAtlas btnReplayTexture = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                64, 56, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TiledTextureRegion btn_pause = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(btnPauseTexture,
                activity, "pause.png", 1, 1);
        TiledTextureRegion btn_play = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(btnPlayTexture,
                activity, "play.png", 1, 1);
        TiledTextureRegion btn_replay = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(btnReplayTexture,
                activity, "replay.png", 1, 1);

        buttonsMAP.put(Constants.BTN_PLAY, btn_play);
        buttonsMAP.put(Constants.BTN_PAUSE, btn_pause);
        buttonsMAP.put(Constants.BTN_REPLAY, btn_replay);

        try {
            btnPauseTexture.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
            btnPlayTexture.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
            btnReplayTexture.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));

            btnPlayTexture.load();
            btnReplayTexture.load();
            ;
            btnPauseTexture.load();
        } catch (ITextureAtlasBuilder.TextureAtlasBuilderException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public void loadGameAudio() {

    }

    public void loadLoadingScreen() {

    }

    ;

    public void unloadLoadingScreen() {

    }

    private void loadAllDucksTTR() {

        //first duck
        BuildableBitmapTextureAtlas firstDuckBBTA = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                1536, 140, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TiledTextureRegion firstDuckTTR = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
                firstDuckBBTA, activity, "duckanimated.png", 6, 1);
        //first duck dead body wuuu
        BuildableBitmapTextureAtlas firstDeadBBTA = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                1536, 286, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TiledTextureRegion firstDeadTTR = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
                firstDeadBBTA, activity, "duckanimateddead.png", 6, 1);
        duckTextureList.add(new DuckTexture(firstDuckTTR, firstDeadTTR));

        //second duck
        BuildableBitmapTextureAtlas secondDuckBBTA = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                1536, 160, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TiledTextureRegion secondDuckTTR = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
                secondDuckBBTA, activity, "duckanimated01.png", 6, 1);
        //second duck dead body wuuu
        BuildableBitmapTextureAtlas secondDeadBBTA = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                1536, 264, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TiledTextureRegion secondDeadTTR = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
                secondDeadBBTA, activity, "duckanimateddead01.png", 6, 1);
        duckTextureList.add(new DuckTexture(secondDuckTTR, secondDeadTTR));


        //third duck
        BuildableBitmapTextureAtlas thirdDuckBBTA = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                1536, 141, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TiledTextureRegion thirdDuckTTR = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
                thirdDuckBBTA, activity, "duckanimated02.png", 6, 1);
        //third duck dead body wuuu
        BuildableBitmapTextureAtlas thirdDeadBBTA = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                1536, 256, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TiledTextureRegion thirdDeadTTR = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
                thirdDeadBBTA, activity, "duckanimateddead02.png", 6, 1);
        duckTextureList.add(new DuckTexture(thirdDuckTTR, thirdDeadTTR));

        //fourth duck
        BuildableBitmapTextureAtlas fourthDuckBBTA = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                1536, 165, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TiledTextureRegion fourthDuckTTR = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
                fourthDuckBBTA, activity, "duckanimated03.png", 6, 1);
        //third duck dead body wuuu
        BuildableBitmapTextureAtlas fourthDeadBBTA = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                1536, 254, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TiledTextureRegion fourthDeadTTR = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
                fourthDeadBBTA, activity, "duckanimateddead03.png", 6, 1);
        duckTextureList.add(new DuckTexture(fourthDuckTTR, fourthDeadTTR));

        //fifth duck
        BuildableBitmapTextureAtlas fifthDuckBBTA = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                1536, 140, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TiledTextureRegion fifthDuckTTR = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
                fifthDuckBBTA, activity, "duckanimated04.png", 6, 1);
        //fifth duck dead body wuuu
        BuildableBitmapTextureAtlas fifthDeadBBTA = new BuildableBitmapTextureAtlas(engine.getTextureManager(),
                1536, 254, TextureOptions.BILINEAR_PREMULTIPLYALPHA);
        TiledTextureRegion fifthDeadTTR = BitmapTextureAtlasTextureRegionFactory.createTiledFromAsset(
                fifthDeadBBTA, activity, "duckanimateddead04.png", 6, 1);
        duckTextureList.add(new DuckTexture(fifthDuckTTR, fifthDeadTTR));

        try {
            firstDuckBBTA.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
            firstDeadBBTA.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
            secondDuckBBTA.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
            secondDeadBBTA.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
            thirdDuckBBTA.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
            thirdDeadBBTA.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
            fourthDuckBBTA.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
            fourthDeadBBTA.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
            fifthDuckBBTA.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
            fifthDeadBBTA.build(new BlackPawnTextureAtlasBuilder<IBitmapTextureAtlasSource, BitmapTextureAtlas>(0, 0, 0));
            firstDuckBBTA.load();
            firstDeadBBTA.load();
            secondDuckBBTA.load();
            secondDeadBBTA.load();
            thirdDuckBBTA.load();
            thirdDeadBBTA.load();
            fourthDuckBBTA.load();
            fourthDeadBBTA.load();
            fifthDuckBBTA.load();
            fifthDeadBBTA.load();
        } catch (ITextureAtlasBuilder.TextureAtlasBuilderException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

}
