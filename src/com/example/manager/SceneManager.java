package com.example.manager;

import android.util.Log;
import com.example.Constants.SceneType;
import com.example.scene.BaseScene;
import com.example.scene.GameScene;
import com.example.scene.LoadingScene;
import com.example.scene.MenuScene;
import org.andengine.engine.Engine;

/**
 * Created with IntelliJ IDEA.
 * User: Win7
 * Date: 8/29/13
 * Time: 2:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class SceneManager {
    private static final String TAG = "SceneManager";

    public BaseScene splashScene;
    public BaseScene menuScene;
    public BaseScene gameScene;
    public BaseScene loadingScene;

    private static final SceneManager INSTANCE = new SceneManager();

    private SceneType currentSceneType = SceneType.SCENE_SPLASH;

    private BaseScene currentScene;

    private Engine engine = ResourceManager.getInstance().engine;

    public void setScene(BaseScene scene) {
        if(currentScene!=null) {
            currentScene.disposeScene();
            currentScene.detachSelf();
        }
        engine.setScene(scene);
        currentScene = scene;
        currentSceneType = scene.getSceneType();
    }

    public void setScene(SceneType sceneType){
        switch (sceneType) {
            case SCENE_MENU:
                setScene(menuScene);
                break;
            case SCENE_GAME:
                setScene(gameScene);
                break;
            case SCENE_LOADING:
                setScene(loadingScene);
                break;
            case SCENE_SPLASH:
                setScene(splashScene);
                break;
            default:
                break;
        }
    }

    public static SceneManager getInstance() {
        return INSTANCE;
    }

    public SceneType getCurrentSceneType(){
        return currentSceneType;
    }

    public BaseScene getCurrentScene() {
        return currentScene;
    }


    /**
     * creating a new instance of @LoadingScene
     */
    public void createLoadingScene(){
        //loading resources
        ResourceManager.getInstance().loadLoadingScreen();
        loadingScene = new LoadingScene();
        setScene(SceneType.SCENE_LOADING);
        Log.i(TAG,"invoked createLoadingScene");
    }

    /**
     * creating a new instance of @MenuScene
     */
    public void createMenuScene() {
        menuScene = new MenuScene();
        //disposing the loading screen, we dont need it anymore
        loadingScene.disposeScene();
        setScene(SceneType.SCENE_MENU);
        Log.i(TAG,"invoked createMenuScene");
    }

    /**
     * creating new instance of GameScene
     */
    public void createGameScene(){
        gameScene = new GameScene();
        setScene(gameScene);
        Log.i(TAG,"invoked createGameScene");
    }

    public void disposeLoadingScene() {
        ResourceManager.getInstance().unloadLoadingScreen();
    }


}
