package com.example.model;

import android.util.Log;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.example.ColorsPack;
import com.example.Constants;
import com.example.controller.BaseGameController;
import com.example.controller.BaseShooterController;
import com.example.manager.ResourceManager;
import com.example.manager.SceneManager;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.input.touch.TouchEvent;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: ALabrosk
 * Date: 9/10/13
 * Time: 2:13 AM
 */
public class BonusElement {
    private static final String TAG = "BonusElement";
    private int whereY, whereX, ElementHeight, ElementWidth;
    private Constants.WeaponType weaponType;
    private int speed;
    private boolean updateMovement = true;
    private PhysicsWorld physicsWorld;
    private Body body;
    private Scene mScene;
    private int timer = 0;
    /*
    for now i use rectangle
     */
    private Rectangle element;

    public BonusElement(Constants.WeaponType weaponType) {
        this.weaponType = weaponType;
        this.physicsWorld = BaseGameController.getInstance().physicsWorld;
        mScene = SceneManager.getInstance().gameScene;
        speed = BaseGameController.getInstance().createSpeed();
        init();
    }

    private void init() {
        Random random = new Random();
        createHeightWidth(random);
        setStartingCoords(random);
    }

    public void createEssentials() {
        element = new Rectangle(whereX, whereY, ElementWidth, ElementHeight, ResourceManager.getInstance().vbom) {
            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                Log.i(TAG, "touched");
                try {
                    //mScene.unregisterTouchArea(this);
                    mScene.detachChild(this);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                BaseShooterController.getINSTANCE().changeWeapon(weaponType);
                return false;
            }
        };
        element.setColor(ColorsPack.green);

        FixtureDef fixtureDef = PhysicsFactory.createFixtureDef(0.0f, 0.5f, 0.6f);
        body = PhysicsFactory.createCircleBody(this.physicsWorld, element, BodyDef.BodyType.KinematicBody,
                fixtureDef);
        body.setLinearVelocity(speed, 0);
        this.physicsWorld.registerPhysicsConnector(new PhysicsConnector(element, body) {
            @Override
            public void onUpdate(float pSecondsElapsed) {
                super.onUpdate(pSecondsElapsed);    //To change body of overridden methods use File | Settings | File Templates.
                mainchecker();
                timer++;
                if (timer % 20 == 0) {
                    Random random = new Random();
                    int counter = random.nextInt(11);
                    float x, y;
                    x = body.getLinearVelocity().x;
                    y = body.getLinearVelocity().y;

                    if (counter < 3) {
                        y = changeY(y, Constants.GoTop);
                    } else {
                        if (counter > 7) {
                            y = changeY(y, Constants.GoBottom);
                        } else {
                            y = changeY(y, Constants.GoStraight);
                            //Log.i("duck onUpdate PhysicsConnector","counter: " + counter + ",secondsElapsed: " + pSecondsElapsed );
                        }
                    }
                    body.setLinearVelocity(x, y);
                }
            }
        });

    }

    public void attachElement() {
        mScene.attachChild(element);
        mScene.registerTouchArea(element);
    }

    //this is not going to allow the duck to go up or down and if the duck is outside the screen kill it for sure
    private void mainchecker() {
        int header = Math.round(Constants.HEADER_BORDER * Constants.CAMERA_HEIGHT / Constants.OLD_CAMERA_HEIGHT);
        int headerOffset = 10;
        if (element.getY() < header + headerOffset) {
            float y1 = changeY(body.getLinearVelocity().y, Constants.GoStraight);
            body.setLinearVelocity(body.getLinearVelocity().x, y1);
            //Log.i(TAG,"invoked MainChecker <");
        } else {
            if (element.getY() > Constants.CAMERA_HEIGHT - headerOffset - Constants.FOOTER_BORDER * Constants.CAMERA_HEIGHT /
                    Constants.OLD_CAMERA_HEIGHT - ElementHeight) {
                float y2 = changeY(body.getLinearVelocity().y, Constants.GoStraight);
                body.setLinearVelocity(body.getLinearVelocity().x, y2);
                //Log.i(TAG,"invoked MainChecker >");
            }
        }
        if (element.getX() > Constants.CAMERA_WIDTH) {
            physicsWorld.unregisterPhysicsConnector(
                    physicsWorld.getPhysicsConnectorManager().findPhysicsConnectorByShape(element));
            physicsWorld.destroyBody(body);
            mScene.detachChild(element);
            mScene.unregisterTouchArea(element);
            element.dispose();
        }

    }

    //where should the duck drive?
    private float changeY(float y, int where) {
        float newy = y;
        switch (where) {
            case Constants.GoBottom: {
                if (y <= 0 && y > -2) {
                    newy++;
                } else {
                    if (y > 0 && y <= 3) {
                        newy = newy + 4;
                    } else {
                        if (y > 3 && y < 10) {
                            newy = newy + 6;
                        }
                    }
                }
                break;
            }
            case Constants.GoTop: {
                if (y >= 0 && y <= 2) {
                    newy--;
                } else {
                    if (y < 0 && y >= -3) {
                        newy = newy - 4;
                    } else {
                        if (y < -3 && y > -10) {
                            newy = newy - 6;
                        }
                    }
                }
                break;
            }
            case Constants.GoStraight: {
                if (y < 3 && y > -3) {
                    newy = 0;
                } else {
                    if (y >= 3) {
                        newy = newy - 4;
                    } else {
                        if (y <= -3) {
                            newy = newy + 4;
                        }
                    }
                }
                break;
            }
        }
        return newy;
    }

    private void createHeightWidth(Random random) {
        float divider = 1.5f;
        int width = Math.round((random.nextInt(60) + 30) * Constants.CAMERA_WIDTH / Constants.OLD_CAMERA_WIDTH) + 30;
        float height = (float) width / divider;
        //Log.i(TAG,"createDuckHeightWidth  width="+width+",height="+height);
        this.ElementWidth = width;
        this.ElementHeight = Math.round(height);
    }

    private void setStartingCoords(Random random) {
        //int whereY=random.nextInt(Constants.CAMERA_HEIGTH - 3 * DuckHeight);
        int header = Math.round(Constants.HEADER_BORDER * Constants.CAMERA_HEIGHT / Constants.OLD_CAMERA_HEIGHT);
        whereY = random.nextInt(Math.round(Constants.CAMERA_HEIGHT - (ElementHeight + 2 * header + Constants
                .FOOTER_BORDER
                * Constants.CAMERA_HEIGHT / Constants.OLD_CAMERA_HEIGHT))) + header;
        whereX = -200;
        whereX = whereX - random.nextInt(100);

        Log.v(TAG, "invoke setStartingCoords - whereY: " + whereY + ", whereX:" + whereX);
    }
}
