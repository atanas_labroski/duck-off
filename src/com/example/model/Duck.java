package com.example.model;

import android.util.Log;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.example.Constants;
import com.example.controller.BaseGameController;
import com.example.manager.ResourceManager;
import com.example.manager.SceneManager;
import com.example.service.TextWriter;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.shape.IShape;
import org.andengine.entity.sprite.AnimatedSprite;
import org.andengine.extension.physics.box2d.PhysicsConnector;
import org.andengine.extension.physics.box2d.PhysicsFactory;
import org.andengine.extension.physics.box2d.PhysicsWorld;
import org.andengine.opengl.texture.region.TiledTextureRegion;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Win7
 * Date: 9/1/13
 * Time: 12:05 AM
 * To change this template use File | Settings | File Templates.
 */
public class Duck extends DuckTexture {
    private static final String TAG = "DUCK";
    public int duckNumber = 1;
    public int DuckHeight;
    public int DuckWidth;
    private Duck duck = this;
    private ResourceManager resourceManager;
    private BaseGameController baseGameController;
    private Body body;
    private PhysicsWorld physicsWorld;
    private Scene mScene;
    private AnimatedSprite sprite;
    private FixtureDef defaultFixtureDef;
    private TiledTextureRegion mainTiledTextureRegion, deadTiledTextureRegion;
    private int speed = 10;
    private int animateSpeed = 80;
    private float divider = 1.5f;
    private float deadDivider = 1.5f;
    private int deadOffset = 30;
    private int whereX;
    private int whereY;
    private boolean updateMovement = true;
    private int timer = 0;
    private int previousTimer = 0;
    private int deadTimer = 0;
    private boolean isdead = false;

    /**
     * creates the duck animation textures
     *
     * @param mainDuckTexture
     * @param deadDuckTexture
     */
    public Duck(TiledTextureRegion mainDuckTexture, TiledTextureRegion deadDuckTexture,
                int duckNumber) {
        super(mainDuckTexture, deadDuckTexture);
        baseGameController = BaseGameController.getInstance();
        resourceManager = ResourceManager.getInstance();
        mScene = SceneManager.getInstance().gameScene;
        this.mainTiledTextureRegion = mainDuckTexture;
        this.deadTiledTextureRegion = deadDuckTexture;
        this.duckNumber = duckNumber;
        this.physicsWorld = baseGameController.physicsWorld;
        duckInit();
    }

    public Duck(DuckTexture duckTexture, int duckNumber) {
        super(duckTexture.getMainDuckTexture(), duckTexture.getDeadDuckTexture());
        baseGameController = BaseGameController.getInstance();
        resourceManager = ResourceManager.getInstance();
        mScene = SceneManager.getInstance().gameScene;
        this.mainTiledTextureRegion = duckTexture.getMainDuckTexture();
        this.deadTiledTextureRegion = duckTexture.getDeadDuckTexture();
        this.duckNumber = duckNumber;
        this.physicsWorld = baseGameController.physicsWorld;
        duckInit();
    }

    private void duckInit() {
        Random random = new Random();
        //is the duck near or you?
        createDuckHeightWidth(random);
        //setWhere onXY
        setStartingCoords(random);
        //count that a bird is created;
        baseGameController.birdCreated(this);
        speed = baseGameController.createSpeed();
        animateSpeed = animateSpeed + 2 * speed;
    }

    public void createEssentials() {
        sprite = new AnimatedSprite(whereX, whereY, DuckWidth, DuckHeight,
                mainTiledTextureRegion, ResourceManager.getInstance().vbom);
        sprite.animate(animateSpeed);

        //creating the default behavior
        defaultFixtureDef = PhysicsFactory.createFixtureDef(1.0f, 0.5f, 0.8f);
        //the actual body of the element
        body = PhysicsFactory.createCircleBody(this.physicsWorld, sprite,
                BodyDef.BodyType.KinematicBody, defaultFixtureDef);
        body.setLinearVelocity(speed, 0);

        //connecting image with physics
        physicsWorld.registerPhysicsConnector(new PhysicsConnector(sprite, body) {
            @Override
            public void onUpdate(float pSecondsElapsed) {
                super.onUpdate(pSecondsElapsed);    //To change body of overridden methods use File | Settings | File Templates.

                // dont let the duck go over or under the screen
                mainchecker();
                if (updateMovement) {
                    timer++;
                    if (timer % 20 == 0) {
                        Random random = new Random();
                        int counter = random.nextInt(11);
                        float x, y;
                        x = body.getLinearVelocity().x;
                        y = body.getLinearVelocity().y;

                        if (counter < 3) {
                            y = changeY(y, Constants.GoTop);
                        } else {
                            if (counter > 7) {
                                y = changeY(y, Constants.GoBottom);
                            } else {
                                y = changeY(y, Constants.GoStraight);
                                //Log.i("duck onUpdate PhysicsConnector","counter: " + counter + ",secondsElapsed: " + pSecondsElapsed );
                            }
                        }
                        body.setLinearVelocity(x, y);
                    }
                }
                //Log.i("duck onUpdate PhysicsConnector","counter: " + counter + ",secondsElapsed: " + pSecondsElapsed );

            }
        });
    }

    private void setStartingCoords(Random random) {
        //int whereY=random.nextInt(Constants.CAMERA_HEIGTH - 3 * DuckHeight);
        int header = Math.round(Constants.HEADER_BORDER * Constants.CAMERA_HEIGHT / Constants.OLD_CAMERA_HEIGHT);
        whereY = random.nextInt(Math.round(Constants.CAMERA_HEIGHT - (DuckHeight + 2 * header + Constants.FOOTER_BORDER
                * Constants.CAMERA_HEIGHT / Constants.OLD_CAMERA_HEIGHT))) + header;
        whereX = -200;
        whereX = whereX - random.nextInt(100);

        Log.v(TAG, "invoke setStartingCoords - whereY: " + whereY + ", whereX:" + whereX);
    }

    //is the duck hear you?
    private void createDuckHeightWidth(Random random) {
        //setting the divider
        switch (duckNumber) {
            case 1: {
                divider = 1.82f;
                deadDivider = 0.895f;
                deadOffset = 40;
                break;
            }
            case 2: {
                divider = 1.86f;
                deadDivider = 0.969f;
                deadOffset = 35;
                break;
            }
            case 3: {
                divider = 1.81f;
                deadDivider = 1;
                deadOffset = 25;
                break;
            }
            case 4: {
                divider = 1.55f;
                deadDivider = 1;
                deadOffset = 35;
                break;
            }
            case 5: {
                divider = 1.82f;
                deadDivider = 1.127f;
                deadOffset = 10;
                break;
            }
        }

        int width = Math.round((random.nextInt(100) + 80) * Constants.CAMERA_WIDTH / Constants.OLD_CAMERA_WIDTH) + 30;
        float height = (float) width / divider;
        //Log.i(TAG,"createDuckHeightWidth  width="+width+",height="+height);
        this.DuckWidth = width;
        this.DuckHeight = Math.round(height);
    }

    //where is the duck if its dead?
    private void createDeadDuckHeighWidth() {
        DuckHeight = Math.round((float) DuckWidth / deadDivider);
    }

    //this is not going to allow the duck to go up or down and if the duck is outside the screen kill it for sure
    private void mainchecker() {
        int header = Math.round(Constants.HEADER_BORDER * Constants.CAMERA_HEIGHT / Constants.OLD_CAMERA_HEIGHT);
        int headerOffset = 10;
        if (sprite.getY() < header + headerOffset) {
            float y1 = changeY(body.getLinearVelocity().y, Constants.GoStraight);
            body.setLinearVelocity(body.getLinearVelocity().x, y1);
            //Log.i(TAG,"invoked MainChecker <");
        } else {
            if (sprite.getY() > Constants.CAMERA_HEIGHT - headerOffset - Constants.FOOTER_BORDER * Constants.CAMERA_HEIGHT /
                    Constants.OLD_CAMERA_HEIGHT - DuckHeight) {
                float y2 = changeY(body.getLinearVelocity().y, Constants.GoStraight);
                body.setLinearVelocity(body.getLinearVelocity().x, y2);
                //Log.i(TAG,"invoked MainChecker >");
            }
        }
        if (sprite.getX() > Constants.CAMERA_WIDTH) {
            physicsWorld.unregisterPhysicsConnector(
                    physicsWorld.getPhysicsConnectorManager().findPhysicsConnectorByShape(sprite));
            physicsWorld.destroyBody(body);
            mScene.detachChild(sprite);
            mScene.unregisterTouchArea(sprite);
            sprite.dispose();
            //Log.i(TAG,"MINUS POINTS");
            baseGameController.birdPassed(this);
        }

    }

    //add the duck on the scene
    public void attachDuck() {
        mScene.attachChild(sprite);
        mScene.registerTouchArea(sprite);
    }

    //where should the duck drive?
    private float changeY(float y, int where) {
        float newy = y;
        switch (where) {
            case Constants.GoBottom: {
                if (y <= 0 && y > -2) {
                    newy++;
                } else {
                    if (y > 0 && y <= 3) {
                        newy = newy + 4;
                    } else {
                        if (y > 3 && y < 10) {
                            newy = newy + 6;
                        }
                    }
                }
                break;
            }
            case Constants.GoTop: {
                if (y >= 0 && y <= 2) {
                    newy--;
                } else {
                    if (y < 0 && y >= -3) {
                        newy = newy - 4;
                    } else {
                        if (y < -3 && y > -10) {
                            newy = newy - 6;
                        }
                    }
                }
                break;
            }
            case Constants.GoStraight: {
                if (y < 3 && y > -3) {
                    newy = 0;
                } else {
                    if (y >= 3) {
                        newy = newy - 4;
                    } else {
                        if (y <= -3) {
                            newy = newy + 4;
                        }
                    }
                }
                break;
            }
        }
        return newy;
    }

    public void setUpForFleet(int x, int y, int speed, int width, int height) {
        this.whereX = x;
        this.whereY = y;
        this.speed = speed;
        this.DuckWidth = width;
        this.DuckHeight = height;
        this.updateMovement = false;
    }

    public void setUpdateMovement(boolean updateMovement) {
        this.updateMovement = updateMovement;
    }

    public void killThisDuck() {
        //removing the duck
        physicsWorld.unregisterPhysicsConnector(
                physicsWorld.getPhysicsConnectorManager().findPhysicsConnectorByShape(sprite));
        physicsWorld.destroyBody(body);
        mScene.detachChild(sprite);
        mScene.unregisterTouchArea(sprite);
    }

    public int getWhereX() {
        return whereX;
    }

    public void setWhereX(int whereX) {
        this.whereX = whereX;
    }

    public int getWhereY() {
        return whereY;
    }

    public void setWhereY(int whereY) {
        this.whereY = whereY;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getDuckHeight() {
        return DuckHeight;
    }

    public void setDuckHeight(int duckHeight) {
        DuckHeight = duckHeight;
    }

    public int getDuckWidth() {
        return DuckWidth;
    }

    public void setDuckWidth(int duckWidth) {
        DuckWidth = duckWidth;
    }

    /**
     * detecting a collision with a circle
     */
    public boolean circleCollisionDetection(int cx, int cy, int cr) {
        boolean collide = false;

        float x1 = sprite.getX();
        float y1 = sprite.getY();

        float x2 = sprite.getX() + getDuckWidth();
        float y2 = sprite.getY() + getDuckHeight();

        float centerX = (x2 - x1) / 2;
        float centerY = (y2 - y1) / 2;

        float rectD = (float) Math.sqrt((getDuckWidth() * getDuckWidth()) + (getDuckHeight() * getDuckHeight()));
        float distance = (float) Math.sqrt((centerX - cx) * (centerX - cx) + (centerY - cy) * (centerY - cy));
        if (distance > rectD / 2 + cr) {
            collide = false;
            return collide;
        } else {
            if (getDuckHeight() < getDuckWidth()) {
                if (distance < getDuckHeight() / 2 + cr) {
                    collide = true;
                    return collide;
                }
            } else {
                if (distance < getDuckWidth() / 2 + cr) {
                    collide = true;
                    return collide;
                }
            }
        }

        if (cy < centerY) {
            //circle is to the top
            if (cx < x2 && cx > x1) {
                //if(cy + ra)

                if (distance < cr + getDuckHeight() / 2) {
                    collide = true;
                    return collide;
                }
            } else {
                if (cx < x1) {
                    //TODO more fancy calculation with sin and cos

                    if (distance < getDuckWidth() / 2) {
                        collide = true;
                        return collide;
                    }
                } else {

                    if (distance < getDuckWidth() / 2) {
                        collide = true;
                        return collide;
                    }
                }
            }

        } else {
            //circle is to the bottom and on the middle
        }

        return collide;
    }

    public boolean isColliding(IShape shape) {
        if (!isdead) {
            boolean isShoot = sprite.collidesWith(shape);
            if (isShoot) {
                bangIsOnDuck();
            }
            return isShoot;
        } else {
            return false;
        }
    }

    private void bangIsOnDuck() {
        isdead = true;
        //Log.i("invoked onAreaTouched","Duck touched");
        float x = body.getLinearVelocity().x;
        float y = body.getLinearVelocity().y;
        //height for the new animation
        createDeadDuckHeighWidth();
        killThisDuck();
        sprite.dispose();
        Log.i(TAG, "disposed on click");

        //the dead duck is created here
        sprite = new AnimatedSprite(sprite.getX(), sprite.getY() - deadOffset, DuckWidth, DuckHeight,
                deadTiledTextureRegion, resourceManager.vbom);
        FixtureDef deadFixtureDef = PhysicsFactory.createFixtureDef(1.0f, 0.5f, 0.1f);
        body = PhysicsFactory.createBoxBody(physicsWorld, sprite,
                BodyDef.BodyType.KinematicBody, deadFixtureDef);
        body.setLinearVelocity(x - (Math.round(4 * Constants.CAMERA_WIDTH / Constants.OLD_CAMERA_WIDTH)), y);
        physicsWorld.registerPhysicsConnector(new PhysicsConnector(sprite, body) {
            @Override
            public void onUpdate(float pSecondsElapsed) {
                super.onUpdate(pSecondsElapsed);
                deadTimer++;
//                            Log.i(TAG, "dead invoked onUpdate: rotation:" + sprite.getRotation());
//                            sprite.setRotation(sprite.getRotation()+0.1f);
                if (deadTimer % 30 == 0) {
                    float x = body.getLinearVelocity().x;
                    if (x > 3) {
                        x = x - 2;
                    }
                    body.setLinearVelocity(x, body.getLinearVelocity().y + 5);
                }
                if (body.getPosition().y + DuckHeight > Constants.CAMERA_HEIGHT) {
                    physicsWorld.unregisterPhysicsConnector(
                            physicsWorld.getPhysicsConnectorManager().findPhysicsConnectorByShape(sprite));
                    physicsWorld.destroyBody(body);
                    mScene.detachChild(sprite);
                    sprite.dispose();
                    Log.i(TAG, "disposed duck");
                }
            }
        });
        sprite.animate(new long[]{60, 60, 60, 60, 60, 5000});
        mScene.attachChild(sprite);
        //showing the points here
        String showPoints = "+" + baseGameController.birdKilled(duck);
        Log.i(TAG, showPoints);
        if (duckNumber >= 3) {
            TextWriter.getINSTANCE().writeDuckSinglePoints(showPoints, sprite.getX() + (DuckWidth / 2), sprite.getY());
        }
    }
}
