package com.example.model;

import android.util.Log;
import com.example.ColorsPack;
import com.example.Constants;
import com.example.controller.BaseGameController;
import com.example.manager.ResourceManager;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.shape.IShape;
import org.andengine.entity.text.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Win7
 * Date: 8/27/13
 * Time: 10:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class AvailableDucks {
    private static final String TAG = "AvailableDucks";
    private List<Duck> duckList;
    private Scene mScene;
    private Text doubleKill, tripleKill;
    private List<Text> doubleKillList, tripleKillList;

    public AvailableDucks(Scene scene) {
        duckList = new ArrayList<Duck>();
        this.mScene = scene;
        doubleKillList = new ArrayList<Text>();
        tripleKillList = new ArrayList<Text>();
    }

    public AvailableDucks(List<Duck> duckList) {
        this.duckList = duckList;
    }

    public void add(Duck duck) {
        duckList.add(duck);
        Log.i(TAG, "added new duck");
    }

    public void removeAllDucks() {
        try {
            for (int i = 0; i < duckList.size(); i++) {
                duckList.get(i).killThisDuck();
            }
            duckList = new ArrayList<Duck>();
            Log.i(TAG, "remove all ducks");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void killDuck(Duck duck) {
        duckList.remove(duck);
    }

    public void shoot(List<IShape> bangEntityList) {
        Duck duck = null;
        int killsCombo = 0;

        for (int j = 0; j < bangEntityList.size(); j++) {
            IShape bangEntity = bangEntityList.get(j);
            for (int i = 0; i < duckList.size(); i++) {
                duck = duckList.get(i);
                if (duck.isColliding(bangEntity)) {
                    duckList.remove(duck);
                    killsCombo++;
                }
            }
        }

        switch (killsCombo) {
            case 1:
                break;
            case 2: {
                doubleKill = new Text(bangEntityList.get(0).getX(), bangEntityList.get(0).getY(),
                        ResourceManager.getInstance().gameFont, "Double Kill", ResourceManager.getInstance().vbom);
                doubleKill.setScale(1.2f * Constants.CAMERA_WIDTH / Constants.OLD_CAMERA_WIDTH);
                doubleKill.setColor(ColorsPack.green);
                mScene.attachChild(doubleKill);
                doubleKillList.add(doubleKill);
                ResourceManager.getInstance().engine.registerUpdateHandler(new TimerHandler(1.0f,
                        new ITimerCallback() {
                            @Override
                            public void onTimePassed(TimerHandler pTimerHandler) {
                                try {
                                    mScene.detachChild(doubleKillList.get(0));
                                    doubleKillList.get(0).dispose();
                                    doubleKillList.get(0).detachSelf();
                                    doubleKillList.remove(0);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                                ResourceManager.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
                            }
                        }));

                BaseGameController.getInstance().addMultyKills(Constants.MultiKillType.DOUBLE_KILL);
                break;
            }
            //TODO create more than three kills and make them all in one list
            case 3:
            case 4:
            case 5:
            case 6:
            case 7: {
                tripleKill = new Text(bangEntityList.get(0).getX(), bangEntityList.get(0).getY(),
                        ResourceManager.getInstance().gameFont, "Triple Kill", ResourceManager.getInstance().vbom);
                tripleKill.setScale(1.5f * Constants.CAMERA_WIDTH / Constants.OLD_CAMERA_WIDTH);
                tripleKill.setColor(ColorsPack.red);
                tripleKillList.add(tripleKill);
                mScene.attachChild(tripleKill);
                ResourceManager.getInstance().engine.registerUpdateHandler(new TimerHandler(1.0f,
                        new ITimerCallback() {
                            @Override
                            public void onTimePassed(TimerHandler pTimerHandler) {
                                mScene.detachChild(tripleKillList.get(0));
                                try {
                                    tripleKillList.get(0).dispose();
                                    tripleKillList.get(0).detachSelf();
                                    tripleKillList.remove(0);
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                                ResourceManager.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
                            }
                        }));
                BaseGameController.getInstance().addMultyKills(Constants.MultiKillType.TRIPLE_KILL);
                break;
            }
        }

    }
}
