package com.example.model.weapons;

import com.example.ColorsPack;
import com.example.Constants;
import com.example.manager.ResourceManager;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;

/**
 * Created with IntelliJ IDEA.
 * User: ALabrosk
 * Date: 9/5/13
 * Time: 10:23 AM
 */
public abstract class Weapon {

    public Scene mScene;
    private Sprite displayMenu;
    private ITextureRegion weaponTextureRegion;
    private Text bulletsText;
    private int bullets;

    protected Weapon(Scene mScene, ITextureRegion weaponTextureRegion, int bullets) {
        this.mScene = mScene;
        this.weaponTextureRegion = weaponTextureRegion;
        this.bullets = bullets;
    }

    public abstract void shoot(TouchEvent touchEvent);

    public void displayWeapon() {
        displayMenu = new Sprite(0, 0,
                weaponTextureRegion, ResourceManager.getInstance().vbom);
        displayMenu.setHeight(Constants.FOOTER_BORDER - 2);
        displayMenu.setWidth(displayMenu.getHeight() * Constants.WeaponDivider);
        displayMenu.setPosition(Constants.CAMERA_WIDTH - displayMenu.getWidth() - 4, Constants.CAMERA_HEIGHT - displayMenu.getHeight() - 2);
        mScene.attachChild(displayMenu);
        showBullets(bullets);
    }

    public void destroyWeapon() {
        mScene.detachChild(displayMenu);
        displayMenu.detachSelf();
        displayMenu.dispose();
        mScene.detachChild(bulletsText);
        bulletsText.detachSelf();
        bulletsText.dispose();
    }

    public void showBullets(int mBullets) {
        bullets = mBullets;
        if (bulletsText != null) {
            try {
                mScene.detachChild(bulletsText);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        bulletsText = new Text(displayMenu.getX() - 32 * Constants.CAMERA_WIDTH / Constants.OLD_CAMERA_HEIGHT,
                displayMenu.getY(), ResourceManager.getInstance().gameFont, "x" + mBullets,
                ResourceManager.getInstance().vbom);
        bulletsText.setColor(ColorsPack.grey);
        bulletsText.setScale(1.3f * Constants.CAMERA_HEIGHT / Constants.OLD_CAMERA_HEIGHT);
        bulletsText.setPosition(bulletsText.getX(), (displayMenu.getY() - 2 + Constants.CAMERA_HEIGHT) / 2 -
                bulletsText.getHeight() / 2);
        mScene.attachChild(bulletsText);
    }
}
