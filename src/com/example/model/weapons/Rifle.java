package com.example.model.weapons;

import android.util.Log;
import com.example.Constants;
import com.example.controller.BaseGameController;
import com.example.manager.ResourceManager;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.shape.IShape;
import org.andengine.entity.sprite.Sprite;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.texture.region.ITextureRegion;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: ALabrosk
 * Date: 9/5/13
 * Time: 11:27 AM
 */
public class Rifle extends Weapon {
    private static final String TAG = "Rifle";
    private static final int DefaultBulletNumber = 2;
    private static final float DefaultDelay = 0.3f;
    private static final int DefaultRadius = 40;
    private static final float DefaultRefillDelay = 0.8f;
    private static final float DefaultBangDelay = 0.2f;
    IShape bangElement1, bangElement2, bangElement3;
    List<Sprite> shotSpriteList;
    private int bullets;
    private boolean enabled;
    private int radius;
    private float delay;
    private float refillDelay;
    private float bangDelay;

    public Rifle(Scene mScene, ITextureRegion weaponTextureRegion) {
        super(mScene, weaponTextureRegion, DefaultBulletNumber);
        this.bullets = DefaultBulletNumber;
        this.enabled = true;
        this.delay = DefaultDelay;
        this.radius = Math.round(DefaultRadius * Constants.CAMERA_HEIGHT / Constants.OLD_CAMERA_HEIGHT);
        this.refillDelay = DefaultRefillDelay;
        this.bangDelay = DefaultBangDelay;
        this.shotSpriteList = new ArrayList<Sprite>();
    }

    @Override
    public void shoot(TouchEvent touchEvent) {
        if (touchEvent.getAction() == TouchEvent.ACTION_DOWN) {
            if (enabled) {
                if (bullets != 0) {
                    bang(touchEvent.getX(), touchEvent.getY());
                    Log.i(TAG, "Rifle Bang Bang, bullets left: " + bullets);
                    enabled = false;
                    ResourceManager.getInstance().engine.registerUpdateHandler(new TimerHandler(delay, new ITimerCallback() {
                        @Override
                        public void onTimePassed(TimerHandler pTimerHandler) {
                            enabled = true;
                            ResourceManager.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
                        }
                    }));
                }
            }
        }
    }

    public void bang(float x, float y) {
        if (bangElement1 == null) {
            bullets--;
            showBullets(bullets);
            if (bullets == 0) {
                enabled = false;
                refill();
            }
            //Log.i(TAG,"invoked bang(" + x + "," + y + ")" );

            bangElement1 = new Rectangle(x - radius / 2, y - radius / 2, radius, radius,
                    ResourceManager.getInstance().vbom);
            bangElement2 = new Rectangle(x - radius / 2, y - radius / 2, radius, radius,
                    ResourceManager.getInstance().vbom);
            bangElement2.setRotation(33.3f);
            bangElement3 = new Rectangle(x - radius / 2, y - radius / 2, radius, radius,
                    ResourceManager.getInstance().vbom);
            bangElement3.setRotation(66.6f);


            /*
            creating a mesh almost as circle
             */
//            int step = 36;
//            int count = 360 / step;
//            int angle = 0;
//            float mx1 = (float) (radius * Math.cos(Math.toRadians((double) angle)));
//            float my1 = (float) (radius * Math.sin(Math.toRadians((double) angle)));
//
//            List<Float> listDots = new ArrayList<Float>();
//
//            for (int i = 0; i < count; i++) {
//                angle += step;
//                double radians = Math.toRadians((double) angle);
//                float mx = (float) (radius * Math.cos(radians));
//                float my = (float) (radius * Math.sin(radians));
//                listDots.add(mx);
//                listDots.add(my);
//                listDots.add(0.0f);
//                Log.i(TAG, " angle: " + angle + ", x:" + mx + " , y:" + my);
//            }
//
//            IShape meshShape = new Mesh(x,y, ArrayUtils.toFloatArray(listDots), 10, DrawMode.TRIANGLE_FAN,
//                    ResourceManager.getInstance().vbom);
//            //meshShape.setColor(ColorsPack.green);
//            meshShape.setColor(Color.RED);
//            mScene.attachChild(meshShape);

            List<IShape> iShapeList = new ArrayList<IShape>();
            iShapeList.add(bangElement1);
            iShapeList.add(bangElement2);
            iShapeList.add(bangElement3);
            //iShapeList.add(meshShape);

            BaseGameController.getInstance().availableDucks.shoot(iShapeList);

            Sprite shotSprite = new Sprite(x - radius, y - radius, 1.7f * radius, 1.7f * radius,
                    ResourceManager.getInstance().weaponsShotMap.get(Constants.WeaponHuntingRifle),
                    ResourceManager.getInstance().vbom);
            shotSprite.setPosition(x - shotSprite.getWidth() / 2, y - shotSprite.getHeight() / 2);
            Random random = new Random();
            shotSprite.setRotation((float) random.nextInt(360));
            mScene.attachChild(shotSprite);
            shotSpriteList.add(shotSprite);

            ResourceManager.getInstance().engine.registerUpdateHandler(new TimerHandler(bangDelay, new ITimerCallback() {
                @Override
                public void onTimePassed(TimerHandler pTimerHandler) {
                    mScene.detachChild(bangElement1);
                    mScene.detachChild(bangElement2);
                    mScene.detachChild(bangElement3);
                    bangElement1.dispose();
                    bangElement2.dispose();
                    bangElement3.dispose();
                    mScene.detachChild(shotSpriteList.get(0));
                    shotSpriteList.get(0).dispose();
                    shotSpriteList.get(0).detachSelf();
                    shotSpriteList.remove(0);
                    bangElement1 = null;
                    ResourceManager.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
                }
            }));
        }

    }

    public void refill() {
        ResourceManager.getInstance().engine.registerUpdateHandler(new TimerHandler(refillDelay, new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                Log.i(TAG, "invoked refill");
                enabled = true;
                bullets = DefaultBulletNumber;
                showBullets(bullets);
                ResourceManager.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
            }
        }));
    }


}
