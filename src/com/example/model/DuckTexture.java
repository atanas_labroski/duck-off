package com.example.model;

import org.andengine.opengl.texture.region.TiledTextureRegion;

/**
 * Created with IntelliJ IDEA.
 * User: Win7
 * Date: 8/31/13
 * Time: 2:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class DuckTexture {
    private TiledTextureRegion mainDuckTexture;
    private TiledTextureRegion deadDuckTexture;

    /**
     * creates the duck animation textures
     * @param mainDuckTexture
     * @param deadDuckTexture
     */
    public DuckTexture(TiledTextureRegion mainDuckTexture, TiledTextureRegion deadDuckTexture) {
        this.mainDuckTexture = mainDuckTexture;
        this.deadDuckTexture = deadDuckTexture;
    }

    public TiledTextureRegion getDeadDuckTexture() {
        return deadDuckTexture;
    }

    public TiledTextureRegion getMainDuckTexture() {
        return mainDuckTexture;
    }
}
