package com.example.scene;

import android.util.Log;
import com.example.Constants;
import com.example.controller.BaseGameController;
import com.example.manager.ResourceManager;
import org.andengine.engine.camera.Camera;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.primitive.Rectangle;
import org.andengine.entity.scene.background.AutoParallaxBackground;
import org.andengine.entity.scene.background.ParallaxBackground;
import org.andengine.entity.scene.menu.MenuScene;
import org.andengine.entity.scene.menu.item.IMenuItem;
import org.andengine.entity.scene.menu.item.SpriteMenuItem;
import org.andengine.entity.shape.IAreaShape;
import org.andengine.entity.sprite.Sprite;
import org.andengine.entity.text.Text;
import org.andengine.input.touch.TouchEvent;
import org.andengine.opengl.util.GLState;
import org.andengine.util.color.Color;

/**
 * Created with IntelliJ IDEA.
 * User: Win7
 * Date: 8/31/13
 * Time: 12:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class GameScene extends BaseScene implements MenuScene.IOnMenuItemClickListener {
    public static final String TAG = "GameScene";
    public static final Constants.GameType gameType = Constants.GameType.GAME_NORMAL;
    public Sprite headerSprite;
    public IAreaShape footerSprite;
    private BaseGameController baseGameController;
    private Sprite pauseBtnSprite;

    public GameScene() {
        createScene();
        BaseGameController.prepareController(Constants.GameType.GAME_NORMAL, this);
        baseGameController = BaseGameController.getInstance();
        ResourceManager.getInstance().engine.registerUpdateHandler(new TimerHandler(4.0f, new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                //To change body of implemented methods use File | Settings | File Templates.
                baseGameController.startGame();
                ResourceManager.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
            }
        }));

        initPauseBtn();
    }

    @Override
    public void createScene() {
        setBackground();
    }

    @Override
    public void onBackKeyPressed() {
        Log.i(TAG, "onBackKeyPressed");
        //TODO modify back key pressed
        System.exit(0);
    }

    @Override
    public Constants.SceneType getSceneType() {
        return Constants.SceneType.SCENE_GAME;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void disposeScene() {
        this.detachSelf();
        this.dispose();
    }

    private void setBackground() {
        AutoParallaxBackground autoParallaxBackground = new AutoParallaxBackground(0, 0, 0, 5);
        autoParallaxBackground.attachParallaxEntity(new ParallaxBackground.ParallaxEntity(-1.0f,
                new Sprite(0, Constants.HEADER_BORDER * Constants.CAMERA_HEIGHT / Constants.OLD_CAMERA_HEIGHT,
                        (1920 * Constants.CAMERA_HEIGHT) / 640, Constants.CAMERA_HEIGHT,
                        ResourceManager.getInstance().mParallaxLayerBack, ResourceManager.getInstance().vbom) {
                    @Override
                    protected void preDraw(GLState pGLState, Camera pCamera) {
                        super.preDraw(pGLState, pCamera);    //To change body of overridden methods use File | Settings | File Templates.
                        pGLState.enableDither();
                    }
                }));
        float y = (85 * Constants.CAMERA_HEIGHT) / Constants.OLD_CAMERA_HEIGHT;
        Log.i(TAG, " clouds y:" + y);
        autoParallaxBackground.attachParallaxEntity(new ParallaxBackground.ParallaxEntity(-5.0f,
                new Sprite(0, 80, (1920 * y) / 85, y,
                        ResourceManager.getInstance().mParallaxLayerMid, ResourceManager.getInstance().vbom)));
//        autoParallaxBackground.attachParallaxEntity(new ParallaxBackground.ParallaxEntity(-10.0f,
//                new Sprite(0, Constants.CAMERA_HEIGTH - this.mParallaxLayerFront.getHeight(), this.mParallaxLayerFront,
//                        vertexBufferObjectManager)));
        this.setBackground(autoParallaxBackground);

        headerSprite = new Sprite(0, 0,
                Constants.CAMERA_WIDTH, Constants.HEADER_BORDER * Constants.CAMERA_HEIGHT / Constants.OLD_CAMERA_HEIGHT,
                ResourceManager.getInstance().mHeaderTTR, ResourceManager.getInstance().vbom);
        footerSprite = new Rectangle(0, Constants.CAMERA_HEIGHT - Constants.FOOTER_BORDER * Constants.CAMERA_HEIGHT /
                Constants.OLD_CAMERA_HEIGHT,
                Constants.CAMERA_WIDTH, Constants.FOOTER_BORDER * Constants.CAMERA_HEIGHT / Constants
                .OLD_CAMERA_HEIGHT, ResourceManager.getInstance().vbom);
        footerSprite.setColor(new Color(0.0f, 0.0f, 0.0f, 0.0f));
        this.attachChild(headerSprite);
        this.attachChild(footerSprite);
    }

    private void initPauseBtn() {
        pauseBtnSprite = new Sprite(6 * Constants.CAMERA_WIDTH / Constants.OLD_CAMERA_WIDTH,
                Constants.CAMERA_HEIGHT - (48 * Constants.CAMERA_HEIGHT / Constants.OLD_CAMERA_HEIGHT),
                48 * Constants.CAMERA_WIDTH / Constants.OLD_CAMERA_WIDTH,
                42 * Constants.CAMERA_HEIGHT / Constants.OLD_CAMERA_HEIGHT,
                ResourceManager.getInstance().buttonsMAP.get(Constants.BTN_PAUSE), ResourceManager.getInstance().vbom) {
            @Override
            public boolean onAreaTouched(TouchEvent pSceneTouchEvent, float pTouchAreaLocalX, float pTouchAreaLocalY) {
                if (pSceneTouchEvent.getAction() == TouchEvent.ACTION_DOWN) {
                    pauseBtnSprite.setVisible(false);
                    baseGameController.pauseGame();
                    setChildSceneByID(Constants.subSceneType.sub_PAUSE);
                }
                return true;
            }
        };
        this.attachChild(pauseBtnSprite);
        this.registerTouchArea(pauseBtnSprite);
    }

    public void setChildSceneByID(Constants.subSceneType subSceneID) {
        Log.i(TAG, "invoked setChildScene with type:" + subSceneID);
        switch (subSceneID) {
            case sub_PAUSE:
                this.setChildScene(pauseScene(), false, true, true);
                break;
            case sub_THE_END:
                this.setChildScene(theEndScene(), false, true, true);
                break;
        }
    }

    private MenuScene theEndScene() {
        final MenuScene theEndScene = new MenuScene(ResourceManager.getInstance().camera);

        final SpriteMenuItem replayBtnSprite = new SpriteMenuItem(
                3, ResourceManager.getInstance().buttonsMAP.get(Constants.BTN_REPLAY),
                ResourceManager.getInstance().vbom);
        replayBtnSprite.setPosition(Constants.CAMERA_WIDTH / 2 - 32, Constants.CAMERA_HEIGHT / 2);


        Text theEnd = new Text(Constants.CAMERA_WIDTH / 2 - 54, Constants.CAMERA_HEIGHT / 2 - 36,
                ResourceManager.getInstance().gameFont, "THE END", ResourceManager.getInstance().vbom);
        theEnd.setPosition(Constants.CAMERA_WIDTH / 2 - theEnd.getWidth() / 2, theEnd.getY());
        theEnd.setScale(2f * (float) Constants.CAMERA_WIDTH / (float) Constants.OLD_CAMERA_WIDTH);
        try {
            theEndScene.detachChild(theEnd);
        } catch (Exception ex) {
        }
        theEndScene.attachChild(theEnd);
        theEndScene.addMenuItem(replayBtnSprite);
        theEndScene.setBackgroundEnabled(false);
        theEndScene.setOnMenuItemClickListener(this);


        Log.i(TAG, "Created the end Scene");
        return theEndScene;
    }

    private MenuScene pauseScene() {
        final MenuScene pauseScene = new MenuScene(
                ResourceManager.getInstance().camera);

        final SpriteMenuItem playBtnSprite = new SpriteMenuItem(1,
                ResourceManager.getInstance().buttonsMAP.get(Constants.BTN_PLAY), ResourceManager.getInstance().vbom);
        playBtnSprite.setPosition(Constants.CAMERA_WIDTH / 2 - 69, Constants.CAMERA_HEIGHT / 2 - 28);
        pauseScene.addMenuItem(playBtnSprite);

        final SpriteMenuItem replayBtnSprite = new SpriteMenuItem(2,
                ResourceManager.getInstance().buttonsMAP.get(Constants.BTN_REPLAY), ResourceManager.getInstance().vbom);
        replayBtnSprite.setPosition(Constants.CAMERA_WIDTH / 2 + 5, Constants.CAMERA_HEIGHT / 2 - 28);
        pauseScene.addMenuItem(replayBtnSprite);

        pauseScene.setBackgroundEnabled(false);
        pauseScene.setOnMenuItemClickListener(this);

        return pauseScene;
    }

    @Override
    public boolean onMenuItemClicked(MenuScene pMenuScene, IMenuItem pMenuItem, float pMenuItemLocalX, float pMenuItemLocalY) {
        Log.i(TAG, "invoked onMenuItemClicked: id-" + pMenuItem.getID());
        switch (pMenuItem.getID()) {
            case 1: {
                if (this.hasChildScene()) {
                    this.clearChildScene();
                    pauseBtnSprite.setVisible(true);
                    ResourceManager.getInstance().engine.registerUpdateHandler(
                            BaseGameController.getInstance().duckTimerHandler);
                }
                break;
            }
            case 2: {
                if (this.hasChildScene()) {
                    ResourceManager.getInstance().engine.stop();
                    this.clearChildScene();
                    pauseBtnSprite.setVisible(true);
                    initReplay();
                    ResourceManager.getInstance().engine.start();
                }
                break;
            }
            case 3: {
                if (this.hasChildScene()) {
                    ResourceManager.getInstance().engine.stop();
                    this.clearChildScene();
                    pauseBtnSprite.setVisible(true);
                    initReplay();
                    ResourceManager.getInstance().engine.start();
                }
                break;
            }

            default: {
                return false;  //To change body of implemented methods use File | Settings | File Templates.
            }
        }
        return false;
    }

    private void initReplay() {
        createScene();
        baseGameController.restart();
    }
}
