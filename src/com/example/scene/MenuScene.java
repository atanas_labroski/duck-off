package com.example.scene;

import android.util.Log;
import com.example.Constants;
import org.andengine.entity.scene.background.Background;

/**
 * Created with IntelliJ IDEA.
 * User: Win7
 * Date: 8/31/13
 * Time: 11:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class MenuScene extends BaseScene {
    private static final String TAG = "MenuScene";

    public MenuScene() {
        createScene();
    }

    @Override
    public void createScene() {
        setBackground(new Background(0,0,120));
    }

    @Override
    public void onBackKeyPressed() {
        System.exit(0);
        Log.i(TAG, "invoked onBackKeyPressed");
    }

    @Override
    public Constants.SceneType getSceneType() {
        return Constants.SceneType.SCENE_MENU;
    }

    @Override
    public void disposeScene() {
        this.detachSelf();
        this.dispose();
    }

}
