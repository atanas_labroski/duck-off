package com.example.scene;

import android.util.Log;
import com.example.ColorsPack;
import com.example.Constants;
import org.andengine.entity.scene.IOnSceneTouchListener;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.scene.background.Background;
import org.andengine.input.touch.TouchEvent;

/**
 * Created with IntelliJ IDEA.
 * User: Win7
 * Date: 8/29/13
 * Time: 2:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class LoadingScene extends BaseScene implements IOnSceneTouchListener {

    private static final String TAG = "LoadingScene";

    public LoadingScene() {
        createScene();
    }

    @Override
    public void createScene() {
        setBackground(new Background(ColorsPack.green));
    }

    @Override
    public void onBackKeyPressed() {
        Log.i(TAG,"invoked onBackKeyPressed");
    }

    @Override
    public Constants.SceneType getSceneType() {
        return Constants.SceneType.SCENE_LOADING;
    }

    @Override
    public void disposeScene() {
        this.detachSelf();
        this.dispose();
    }

    @Override
    public boolean onSceneTouchEvent(Scene pScene, TouchEvent pSceneTouchEvent) {
        Log.i(TAG,"invoked OnSceneTouchEvent");
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
