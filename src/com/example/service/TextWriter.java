package com.example.service;

import android.util.Log;
import com.example.ColorsPack;
import com.example.Constants;
import com.example.base.GameApplication;
import com.example.controller.BaseGameController;
import com.example.manager.ResourceManager;
import org.andengine.engine.handler.timer.ITimerCallback;
import org.andengine.engine.handler.timer.TimerHandler;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.text.Text;
import org.andengine.opengl.font.Font;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Win7
 * Date: 9/1/13
 * Time: 1:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class TextWriter {
    private static final String TAG = "TextWriter";
    public static TextWriter INSTANCE = new TextWriter();
    private Text scoreText;
    private Text highScoreText;
    private Text multiplierText;
    private Font textWriterFont;
    private Scene mScene;
    private float mainScaleDiv;
    private Constants.GameType gameType;
    private boolean firstDraw = true;
    private float positionScore, positionHighScore, positionMultiplier, positionYScore, positionYHighScore,
            positionYMultiplier;

    private List<Text> listMultyPoints;

    public static void prepareTextWriter(Scene scene, Font font, Constants.GameType gameType) {
        getINSTANCE().mScene = scene;
        getINSTANCE().textWriterFont = font;
        getINSTANCE().mainScaleDiv = (float) Constants.CAMERA_HEIGHT / (float) Constants.OLD_CAMERA_HEIGHT * 0.8f;
        getINSTANCE().gameType = gameType;
        getINSTANCE().positionScore = 10;
        getINSTANCE().listMultyPoints = new ArrayList<Text>();
        init();
    }

    public static void init() {
        getINSTANCE().writePoints(0, false);
        getINSTANCE().writeHighScore(GameApplication.getINSTANCE().getHighScore(getINSTANCE().gameType));
        getINSTANCE().writeMultiplier(BaseGameController.getInstance().multiplier);

    }

    public static TextWriter getINSTANCE() {
        return INSTANCE;
    }

    public void writePoints(long points, boolean isHighScore) {
        if (scoreText != null) {
            mScene.detachChild(scoreText);
        }
        scoreText = new Text(positionScore, 0,
                this.textWriterFont, " Points: " + points + "   ", ResourceManager.getInstance().vbom);
        scoreText.setScale((getINSTANCE().mainScaleDiv * Constants.FONT_SIZE_GAME_HEADER) / Constants.FONT_SIZE_DEFAULT);
        if (firstDraw) {
            positionScore = Constants.CAMERA_WIDTH * 0.03f;
            positionYScore = (Constants.HEADER_BORDER * Constants.CAMERA_HEIGHT / Constants
                    .OLD_CAMERA_HEIGHT - scoreText.getHeight()) / 2;
        }
        scoreText.setPosition(positionScore, positionYScore);
        if (isHighScore) {
            scoreText.setColor(ColorsPack.green);
        } else {
            scoreText.setColor(ColorsPack.grey);
        }
        mScene.attachChild(scoreText);
    }

    public void writeHighScore(long highScore) {
        if (highScoreText != null) {
            mScene.detachChild(highScoreText);
        }
        highScoreText = new Text(0, 0,
                this.textWriterFont, " best: " + highScore + "   ", ResourceManager.getInstance().vbom);
        highScoreText.setColor(ColorsPack.green);
        highScoreText.setScale((getINSTANCE().mainScaleDiv * Constants.FONT_SIZE_GAME) / Constants.FONT_SIZE_DEFAULT);

        if (firstDraw) {
            positionHighScore = Constants.CAMERA_WIDTH * 0.28f;
            //positionHighScore = getINSTANCE().scoreText.getX() + getINSTANCE().scoreText.getWidth() + 25;
            positionYHighScore = (Constants.HEADER_BORDER * Constants.CAMERA_HEIGHT / Constants
                    .OLD_CAMERA_HEIGHT - highScoreText.getHeight()) / 2;
        }
        highScoreText.setPosition(positionHighScore, positionYHighScore);
        mScene.attachChild(highScoreText);
        Log.i(TAG, "writing new High Score:" + highScore);
    }

    public void writeMultiplier(int multiplier) {
        if (multiplierText != null) {
            mScene.detachChild(multiplierText);
        }
        multiplierText = new Text(0, 0,
                textWriterFont, "x" + multiplier, ResourceManager.getInstance().vbom);
        multiplierText.setScale((getINSTANCE().mainScaleDiv * Constants.FONT_SIZE_GAME_HEADER) / Constants.FONT_SIZE_DEFAULT);
        if (firstDraw) {
            positionMultiplier = Constants.CAMERA_WIDTH * 0.48f;
            //positionMultiplier = 25 + getINSTANCE().highScoreText.getX() + getINSTANCE().highScoreText.getWidth();
            positionYMultiplier = positionYScore;
            //the first draw is set to false so we wont calculate again
            firstDraw = false;
        }
        multiplierText.setPosition(positionMultiplier, positionYMultiplier);
        switch (multiplier) {
            case 1:
            case 2:
            case 3: {
                multiplierText.setColor(ColorsPack.grey);
                break;
            }
            case 4:
            case 5: {
                multiplierText.setColor(ColorsPack.green);
                break;
            }
            case 6:
            case 7: {
                multiplierText.setColor(ColorsPack.red);
                break;
            }

        }
        mScene.attachChild(multiplierText);
    }

    public void writeDuckSinglePoints(String showPoints, float x, float y) {
        final Text text = new Text(x, y, textWriterFont,
                showPoints, ResourceManager.getInstance().vbom);
        text.setColor(ColorsPack.green);
        text.setScale((getINSTANCE().mainScaleDiv * Constants.FONT_SIZE_GAME) / Constants.FONT_SIZE_DEFAULT);
        mScene.attachChild(text);

        TimerHandler timerHandler = new TimerHandler(0.5f, false, new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                try {
                    ResourceManager.getInstance().engine.unregisterUpdateHandler(pTimerHandler);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                mScene.detachChild(text);
            }
        });
        mScene.registerUpdateHandler(timerHandler);
    }

    public void restart() {
        writeHighScore(GameApplication.getINSTANCE().getHighScore(gameType));
        writeMultiplier(BaseGameController.getInstance().multiplier);
        writePoints(0, false);
    }

    public void writeMultiKillPoints(int points, Constants.MultiKillType type) {
        final Text text = new Text(scoreText.getX(), 3 + Constants.HEADER_BORDER * Constants.CAMERA_HEIGHT / Constants
                .OLD_CAMERA_HEIGHT, textWriterFont, " +" + points, ResourceManager.getInstance().vbom);
        text.setScale((getINSTANCE().mainScaleDiv * Constants.FONT_SIZE_GAME_HEADER) / Constants.FONT_SIZE_DEFAULT);
        switch (type) {
            case DOUBLE_KILL:
                text.setColor(ColorsPack.green);
                break;
            case TRIPLE_KILL:
                text.setColor(ColorsPack.red);
                break;
            case QUADRA_KILL:
                //todo set quadra kill color
                break;
        }
        listMultyPoints.add(text);
        mScene.attachChild(text);
        ResourceManager.getInstance().engine.registerUpdateHandler(new TimerHandler(1.0f, new ITimerCallback() {
            @Override
            public void onTimePassed(TimerHandler pTimerHandler) {
                try {
                    mScene.detachChild(listMultyPoints.get(0));
                    listMultyPoints.get(0).detachSelf();
                    listMultyPoints.get(0).dispose();
                    listMultyPoints.remove(0);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }));
    }


}
