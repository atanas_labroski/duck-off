package com.example.service;

import com.example.Constants;
import com.example.manager.ResourceManager;
import org.andengine.entity.scene.Scene;
import org.andengine.entity.sprite.Sprite;

/**
 * Created with IntelliJ IDEA.
 * User: ALabrosk
 * Date: 9/2/13
 * Time: 12:32 PM
 */
public class Lives {
    private static final String TAG = "Lives";

    public static Lives INSTANCE = new Lives();

    public int lives;
    private Scene mScene;
    public Constants.GameType gameType;

    private Sprite zero;
    private Sprite one;
    private Sprite two;
    private Sprite three;
    public int Height = 24;
    public int Width = 120;

    private Sprite attachedSprite;

    public static void prepareLives(Scene mScene, Constants.GameType gameType) {
        getINSTANCE().gameType = gameType;
        getINSTANCE().lives = 3;
        getINSTANCE().mScene = mScene;
        getINSTANCE().Height = Math.round(getINSTANCE().Height * Constants.CAMERA_HEIGHT / Constants.OLD_CAMERA_HEIGHT);
        getINSTANCE().Width = Math.round(getINSTANCE().Width * Constants.CAMERA_WIDTH /Constants.OLD_CAMERA_WIDTH);

        getINSTANCE().zero = new Sprite(Constants.CAMERA_WIDTH - 10 - getINSTANCE().Width,
                Constants.HEADER_BORDER_TEXT_Y * Constants.CAMERA_HEIGHT / Constants.OLD_CAMERA_HEIGHT,
                getINSTANCE().Width, getINSTANCE().Height,
                ResourceManager.getInstance().livesList.get(0), ResourceManager.getInstance().vbom);
        getINSTANCE().one = new Sprite(Constants.CAMERA_WIDTH - 10 - getINSTANCE().Width,
                Constants.HEADER_BORDER_TEXT_Y * Constants.CAMERA_HEIGHT / Constants.OLD_CAMERA_HEIGHT,
                getINSTANCE().Width, getINSTANCE().Height,
                ResourceManager.getInstance().livesList.get(1), ResourceManager.getInstance().vbom);
        getINSTANCE().two = new Sprite(Constants.CAMERA_WIDTH - 10 - getINSTANCE().Width,
                Constants.HEADER_BORDER_TEXT_Y * Constants.CAMERA_HEIGHT / Constants.OLD_CAMERA_HEIGHT,
                getINSTANCE().Width, getINSTANCE().Height,
                ResourceManager.getInstance().livesList.get(2), ResourceManager.getInstance().vbom);
        getINSTANCE().three = new Sprite(Constants.CAMERA_WIDTH - 10 - getINSTANCE().Width,
                Constants.HEADER_BORDER_TEXT_Y * Constants.CAMERA_HEIGHT / Constants.OLD_CAMERA_HEIGHT,
                getINSTANCE().Width, getINSTANCE().Height,
                ResourceManager.getInstance().livesList.get(3), ResourceManager.getInstance().vbom);

        switch (gameType) {
            case GAME_NORMAL:
                getINSTANCE().setLives(3);
                break;
            case GAME_ARCADE:
                getINSTANCE().setLives(3);
                break;
        }
    }

    public static Lives getINSTANCE() {
        return INSTANCE;
    }

    private void setLivesSprite(Sprite sprite) {
        if(attachedSprite!=null){
            mScene.detachChild(attachedSprite);
        }
        mScene.attachChild(sprite);
        attachedSprite = sprite;
    }

    public void setLives(int lives) {
        switch (lives) {
            case 0:
                setLivesSprite(zero);
                break;
            case 1:
                setLivesSprite(one);
                break;
            case 2:
                setLivesSprite(two);
                break;
            case 3:
                setLivesSprite(three);
                break;
            default:
                break;
        }
        this.lives = lives;
    }


    public boolean equals(int i) {
        return lives == i;    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void minusLive(){
        lives--;
        switch (lives) {
            case 0: {
                setLivesSprite(zero);
                break;
            }
            case 1: {
                setLivesSprite(one);
                break;
            }
            case 2: {
                setLivesSprite(two);
                break;
            }
            case 3: {
                setLivesSprite(three);
                break;
            }
        }
    }

    public void plusLive() {
        lives++;
        if(lives>3) {
            lives = 3;
            return;
        }else {
            switch (lives) {
                case 0: {
                    setLivesSprite(zero);
                    break;
                }
                case 1: {
                    setLivesSprite(one);
                    break;
                }
                case 2: {
                    setLivesSprite(two);
                    break;
                }
                case 3: {
                    setLivesSprite(three);
                    break;
                }
            }
        }
    }
}
