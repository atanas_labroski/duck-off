package com.example.service;

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: Win7
 * Date: 8/25/13
 * Time: 10:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class Generator {

    public static int generateDuckNumber(int size, Random random) {
        random = new Random();
        int rNumber =  random.nextInt(100) + 1;

        //Log.i("generateDuckNumber"," rNumber: " + rNumber);

        if(rNumber < 70) {
            return 0;
        } else {
            if(rNumber < 83) {
                if(size != 1) {
                    return 1;
                } else return 1;
            } else {
                if(rNumber < 90) {
                    if(size != 1 && size != 2) {
                        return 2;
                    } else return 1;
                } else {
                    if(rNumber < 96) {
                        if(size != 1 && size != 2 && size != 3) {
                            return 3;
                        } else return 0;
                    } else {
                        if(size != 1 && size != 2 && size != 3 && size != 4) {
                            return 4;
                        } else return 1;
                    }
                }
            }
        }

    }
}
