package com.example.base;

import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.WindowManager;
import com.example.Constants;

/**
 * Created with IntelliJ IDEA.
 * User: Win7
 * Date: 8/31/13
 * Time: 11:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class GameApplication extends Application {
    private static final String TAG = "GameApplication";
    public static GameApplication INSTANCE;

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        Log.i(TAG, "GameApplication onCreate created new GameApplication INSTANCE");

        sharedPreferences = this.getSharedPreferences(Constants.GamePreferencesHS,0);
        editor = sharedPreferences.edit();

        WindowManager windowManager = (WindowManager)getSystemService(WINDOW_SERVICE);
        Constants.CAMERA_WIDTH = windowManager.getDefaultDisplay().getWidth();
        Constants.CAMERA_HEIGHT = windowManager.getDefaultDisplay().getHeight();
        if(Constants.CAMERA_HEIGHT > Constants.CAMERA_WIDTH) {
            int a = Math.round(Constants.CAMERA_HEIGHT);
            Constants.CAMERA_HEIGHT = Constants.CAMERA_WIDTH;
            Constants.CAMERA_WIDTH = a;
        }

        Log.i(TAG, "setting up CAMERA_HEIGHT and CAMERA_WIDTH for all devices \n W:" + Constants.CAMERA_WIDTH + " H:" + Constants.CAMERA_HEIGHT);
    }

    public static GameApplication getINSTANCE() {
        return INSTANCE;
    }

    public void saveHighScore(long score, Constants.GameType gameType) {
        String gameMode = "";
        switch (gameType) {
            case GAME_ARCADE:
                gameMode = Constants.GameArcade;
                break;
            case GAME_NORMAL:
                gameMode = Constants.GameNormal;
                break;
        }
        editor.putLong(gameMode,score);
        editor.commit();
        Log.i(TAG,"puttingNewHighScore for mode:" + gameMode + ", score:" + score);
    }

    public long getHighScore(Constants.GameType gameType) {
        String gameMode = "";
        switch (gameType) {
            case GAME_ARCADE:
                gameMode = Constants.GameArcade;
                break;
            case GAME_NORMAL:
                gameMode = Constants.GameNormal;
                break;
        }
        long highScore = sharedPreferences.getLong(gameMode,0);
        Log.i(TAG,"loading high score:" + highScore);
        return highScore;
    }
}
