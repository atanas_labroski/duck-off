package com.example.base;

import android.util.Log;
import android.view.KeyEvent;
import com.example.Constants;
import com.example.manager.HandlerManager;
import com.example.manager.ResourceManager;
import com.example.manager.SceneManager;
import org.andengine.engine.camera.SmoothCamera;
import org.andengine.engine.options.EngineOptions;
import org.andengine.engine.options.ScreenOrientation;
import org.andengine.engine.options.WakeLockOptions;
import org.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.andengine.entity.scene.Scene;
import org.andengine.ui.activity.BaseGameActivity;

public class MainGameActivity extends BaseGameActivity {
    private static final String TAG = "MainGameActivity";

    private SmoothCamera mCamera;
    private ResourceManager resourceManager;
    private HandlerManager handlerManager;
    private SceneManager sceneManager;


//    /**
//     * changing the engine to a more specific (@link)FixedStepEngine that is going to move on a fixed steps
//     * and works on 60 steps per sec
//     * @param pEngineOptions
//     * @return new FixedStepEngine
//     */
//    @Override
//    public Engine onCreateEngine(EngineOptions pEngineOptions) {
//        return new FixedStepEngine(pEngineOptions, 100);    //To change body of overridden methods use File | Settings | File Templates.
//    }

    @Override
    public EngineOptions onCreateEngineOptions() {
        Log.i(TAG,"invoked onCreateEngineOptions");
        mCamera = new SmoothCamera(0, 0, Constants.CAMERA_WIDTH, Constants.CAMERA_HEIGHT, 10, 10, 1.0f);
        //display engine options

        /*
            RatioResolutionPolicy - predefined WIDTH AND HEIGHT
         */
        EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED,
                new RatioResolutionPolicy(Constants.CAMERA_WIDTH, Constants.CAMERA_HEIGHT), this.mCamera);
        /*
            FillResolutionPolicy - fills the screen with the camera dimensions
         */
//        EngineOptions engineOptions = new EngineOptions(true, ScreenOrientation.LANDSCAPE_FIXED,
//                new FillResolutionPolicy(), this.mCamera);
//

        //sound engine options
        engineOptions.getAudioOptions().setNeedsMusic(true).setNeedsSound(true);
        //wake locker engine options
        engineOptions.setWakeLockOptions(WakeLockOptions.SCREEN_ON);
        return engineOptions;
    }

    @Override
    public void onCreateResources(OnCreateResourcesCallback pOnCreateResourcesCallback) throws Exception {
        Log.i(TAG,"invoked onCreateResources");
        ResourceManager.prepareManager(mEngine, this, mCamera, this.getVertexBufferObjectManager());
        resourceManager = ResourceManager.getInstance();
        handlerManager = HandlerManager.getInstance();
        pOnCreateResourcesCallback.onCreateResourcesFinished();
    }

    @Override
    public void onCreateScene(OnCreateSceneCallback pOnCreateSceneCallback) throws Exception {
        Log.i(TAG,"invoked onCreateScene");
        SceneManager.getInstance().createLoadingScene();
        sceneManager = SceneManager.getInstance();
        pOnCreateSceneCallback.onCreateSceneFinished(sceneManager.getCurrentScene());
    }

    @Override
    public void onPopulateScene(Scene pScene, OnPopulateSceneCallback pOnPopulateSceneCallback) throws Exception {
        handlerManager.loadingHandler();
        pOnPopulateSceneCallback.onPopulateSceneFinished();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            SceneManager.getInstance().getCurrentScene().onBackKeyPressed();
        }
        return false;    //To change body of overridden methods use File | Settings | File Templates.
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();    //To change body of overridden methods use File | Settings | File Templates.
    }
}
